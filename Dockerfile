# GitHub:       https://github.com/gohugoio
FROM alpine:3.20

# Configuration variables
ENV HUGO_VERSION=0.130.0-r0 
ENV SITE_DIR='/usr/share/blog'

# libc6-compat & libstdc++ are required for extended SASS libraries
# ca-certificates are required to fetch outside resources (like Twitter oEmbeds)
RUN apk update && \
    apk add --no-cache ca-certificates libc6-compat libstdc++ git && \
    apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo=${HUGO_VERSION} && \
    mkdir ${SITE_DIR}

WORKDIR ${SITE_DIR}

# Expose port for live server
EXPOSE 1313

CMD ["hugo"]
