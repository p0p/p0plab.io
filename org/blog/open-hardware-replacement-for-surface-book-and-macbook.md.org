On a mission to reduce my dependency on the tech giants, including Apple
and Microsoft of course. One of the big problems here is that I'm a
Macbook user and, controversially, a Surface Book 2 user (I got one a
few months ago, attracted to the touchscreen, drawing, tablet options,
it really is quite a bit of tech IMHO a better device than the macbook
with a substandard OS).

What then am I going to do about a new laptop? Something which isn't
apple or Microsoft. First thing I asked myself was is there an Open
Source alternative? - answer yes... and an alternative which may be
better for me for now.

I'll give a quick overview of the Open Source options and then go over
the option I'm trying out

what do I need:

- Quad i7 processor (or similar) - something with some processing power
  for both programming and artwork
- (graphics card not as important for the work I do, I don't render much
  3D at frame rates nor do I play games ).
- Portable - prefer 14"/13" 15" is a maybe, 17" is a no.
- Clear screen, HD
- 16GB ram, for programming and drawing
- 250GB SSD (possibly second SSD)

First up is the System76 and the Galago Pro.

- i7-8265U
- 1980 x 1080 (HD) 14" screen
- 16GB ram
- 250GB SSD (+ 250GB SSD second drive)
- Total: $1,476 (plus shipping + tax) est. $2,000 => £1,532

System76 are a small outfit, in fact most of the open source offerings
for laptops are. They supply a linux distro called Pop OS to expose
controls on the hardware settings etc. There are a number of laptops
available, the one I listed meets my criteria the most.

Next Librem 13 from Purism

- i7-7500U
- 1980 x 1080 (HD) 13.3"
- 16GB ram
- 250GB SSD (+250GB SSD second drive)
- $1,906 (plus shipping + tax) est £2,500 => £1,915

Purism believe "people should have secure devices that protect them
rather than exploit them" - perfect. They also offer their own flavour
of Linux called PureOS to expose controls over the hardware. Purism are
also developing a phone and a phoneOS which I may be interested in in
the near future.

I really like these small boutique companies challenging the laptop
giants with offerings equally as attractive and powerful. It surprises
me that they are so few and far between, I'd imagine there would be
more - maybe it's a very hard market to break into and get right. There
are a couple of things that bother me:

1. The companies are both based in the US, there is no office or
   workshop here in the UK, this means importing and extra charges. But
   it also means any return to base warranty they offer would really not
   be of use as shipping to and from them would cost £££.
2. I'm curious about the purpose of creating an OS just for their
   machines. No doubt it's usable on others too but why? Why not just
   contribute to other distros like Ubuntu or Mint, these machines are
   generic tin with open source motherboards/bios (I assume). It just
   means that they have to manage an overhead to keep their distro up to
   date etc and the effort they spend doing that could have been applied
   to contributing to other OS distros.

There are no serious UK contenders and the risk for now is too great for
me without trying out an alternative. Enter the Lenovo T430... Yes,
Lenovo, our good old Thinkpads (which I've always had a liking for ever
since I owned a T43p in 2005).

Lenovo T430 (Hacked)

- i7-3840QM
- 1920 x 1080 HD 14" screen
- 16GB ram
- 250GB SSD (+250GB SSD)
- Total est £500

This model of Thinkpad is one of the last which can be almost completely
upgraded before they went ultra slim. The spec above will mean upgrading
all the stock parts (differing in levels of complexity). Luckily these
can all be mostly bought second hand for reasonable price. What makes
this even more special is the compatibility with CoreBoot (or Libreboot)
and flashing the bios will give total control over all the hardware
components (maybe even deeper control than the more modern open source
options).

Believe it or not
[[https://www.cpubenchmark.net/compare/Intel-i7-8565U-vs-Intel-i7-7500U-vs-Intel-i7-3840QM/3308vs2863vs900][the
i7-3840QM processor holds its own on benchmark tests]], the difference
is the power consumption (45W for the 3rd gen i7 vs the 15W for the 7th
and 8th gen ones). This transfers to heat of course and the fan that the
i7-3840QM requires means a bigger base unit, it's not an /ulta slim/
option and I'm fine with that, it is still very much a portable laptop
for me.

I am upgrading the laptop as I need to, the above is my target spec and
est cost - currently I've got 12GB ram, a lower spec screen and just a
single 250GB SSD which has cost me total £180 for everything and I
haven't experienced any performance issues yet.

I've been using my T430 for a few weeks now and I have to say, I love
it. The design is just classic, it feels it's built to last, is portable
enough. There's even room in the machine to put some extras - I plan on
adding an arduino and raspberry pi into some of the free unused bays.
[[https://medium.com/@n4ru/the-definitive-t430-modding-guide-3dff3f6a8e2e][Take
a look at this post from an enthusiast who has maxed out upgrading his
T430.]]

Having done only a little upgrading on the machine already it feels like
I know it well and it's design is very accessible, each part is
replaceable (potentially upgradable). Somehow this familiarity with the
machine is creating a connection between me and it - I wonder if I'd get
the same with the Purism or System76 machines (maybe). One thing is for
certain, having owned the Surface Book 2 for a number of months I can
say I almost got the opposite feeling from it - probably because I was
running Windows.

I think the more I do the more I do with the T430 the more I will enjoy
using it. I'll keep a log some place of the updates... My only concern
is that the dated components will in the medium term become obsolete,
then again they stand up to meet the performance of most high spec
modern laptops.

Also for just £360 I bought a Lenovo W530 with extreme i7 (i7-3940XM)
processor and 32 GB ram, 15.6" 1920x1080 HD screen, 500GB SSD and 2GB
Nvidia Quadro Graphics card (though these GPUs do not play nice with
Linux, or rather evil Nvidia have continually updated their drivers so
they won't work on linux!). In benchmark tests it's probably more
powerful than most of the latest laptops (it was from a time when the
spec in laptops was getting a bit silly - termed as "portable
workstations"). for me it's not portable enough for my running around
town machine but for the cost it makes an excellent CAD machine at home
and I'm happy to carry it around on the odd occasion.

The other thing I like about this option is that I am making use of old
technology and precious metals, I reckon we should all start upcycling
old tech more... There's at least another 5 maybe 10 years of high spec
power in these machines for my needs.

For now I'm more than happy (slightly obsessed) with the T430, however,
if I do reach its limites I will seriously consider a System76.
