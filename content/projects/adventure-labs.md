+++
title = "Adventure Labs"
author = ["Tom Broughton"]
date = 2020-04-05T23:14:00+01:00
draft = true
+++

In 2013-2015 I was the Technical Architect for the BBC's Make it
Digital season which brought a myriad of computer science content and
experiences to the youth of the UK (and their parents).  During this
time I studied computational thinking and creative learning pedegogy
including the work of Seymour Papert, Sugata Mitra and Sir Ken
Robinson.  The recurring theme throughout their findings is learning
through doing and Adventure Labs brings computer science learning
through solving real world problems in outdoor adventure settings.
