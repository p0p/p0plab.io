---
title: "Phone"
date: 2019-01-25T23:48:27Z
description: ""
draft: true
---

What about the phone?

Operating system

* android
* plasma mobile
* ubuntu touch
* lineageOS
* pure os

Software

* [f-android](https://f-droid.org/)
* [tresoit alternative to google docs](https://tresorit.com/)
* [ehterpad remote collaboration on a doc](http://etherpad.org)


phone

* nexus 5 - might be good to get going with
* oneplus one
* fairphone 2 - great looking company, something I would support.
* purism
