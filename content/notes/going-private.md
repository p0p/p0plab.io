---
title: "Going Private"
date: 2019-01-23T20:39:23Z
description: ""
draft: true
---

I shop at Amazon, run my web services in Amazon, watch the odd film and listen to audio books on Amazon, read books bought direct from Amazon on my Amazon produced e-reader.

I've owned both iPhones and Android phones, both require an account from Apple or Google respectively - the terms allow access to all your data.

I often find myself in vendor wars, caught between the cross-fire of tech giants wanting you to leave the opponent for their services. Windows runs Google products suboptimally and when  installed the windows app to link to my pc it turned my android phone into a look-a-like microsoft one.

I once paid for a coffee and was told I could pay using my phone by the barrister, I said I'd never done set that up... An hour or so later I received a google pay notification direct to my phone, the first and last.

The news is rife with sensation and stories about how our data is used in questionable ways.  Tech giants are frequently in court and sued large amounts for breeching data laws.  

So are these free to use products and services as free as they seem?  Are we paying a price we don't fully understand?  And what are the alternatives?

New stories, political campaign, underhand use of personal data.

philosophically - digital existentialism - my personal data is me.






I believe that our personal data is a precious digital commodity and that we need to be able to control what our data is, who has access to it and what we allow it to be used for.

The internet should be freely open and accessible to everyone and everyone should be safe on it.  We should be able to be citizens of the web without our data being used in ways we don't clearly understand or collected and distributed without our consent.

We can't depend on others to protect our privacy so we should do this ourselves.
d products where the use of data is clear and easy to understand.  

Use services that are inter-operable and transferable, i.e. where it's possible for my data to be transferred and repurposed.

I've decided to take back control of my privacy whilst embracing openness more.  These are support two things I strongly believe in:

* our data is our own it's the new currency in the digital age and it is a very precious commodity. We should be the agents of it, we should protect ourselves from it being used in ways we do not want it used abuse of it and we need to develop the tools and processes to manage it and make it safe - easily, for everyone.
* the internet should be open and accessible, it should be a trusted space which we feel free to be open within (as long as we are suitably protected), we shouldn't be

Principles:

1. providers should be clear and simple how data is used
1. anonymous tracking
1. data shouldn't be sold to others
1. payment for service in exchange for data control

I've become very lazy, very lazy and very much a consumer of shiny digital products and services which come at a _hidden_ cost.  I say _hidden_ because although legally companies have to inform you and seek consent to use your personal data they often don't make it clear just how they use it, where and when they access it, what data they combine and from who else they receive data about you and who exactly they share your data with.  Ultimately it is unknown what impact it has on _you_.

In the news we repeatedly see abuse of personal data, manipulation of hearts and minds, mishaps and accidents leading to exposing what we thought would be private.

good intentions gone wrong One thing is for certain: Your data is used to manipulate you for commercial and political gain - it's about hidden control on an industrial scale far beyond our comprehension.  Is that a fair price for the shiny easy life they offer you?  For me the answer is no.

I'm on a mission to explore the alternatives to the main stream digital products and services.  To protect myself and my family as much as possible from the influences of large corporate and capitalist control by removing my dependency on the service and products they supply.  

It used to be the case that the cost of using a service was financial.  This is sometimes the case but more often than not we are able to subscribe and use products and services that would have once cost us money... So is this really free?

1. Hardware
  1. computer / Latptop
  1. mobile phones
1. Operating system
1. Browsing Internet
  1. Browser
  1. ISP
  1. Sites (cookies)
1. Email
1. Social media
1. Cloud backup
1. Software, products and services
  1. Office suite
  1. Drawing
  1. Photo editing
  1. Vector Drawing
  1. Diagrams
  1. Productivity/time management
  1. Maps and navigation
  1. Music & Video (entertainments)
  1. password management
1. Shopping
1. Instant messaging
1. Cloud provider (PaaS/IaaS)
1. Banking & Accounts
1. books


Devices:

1. Latptop / PC
1. Phone
1. Tablet (other devices)
1. Watch
1. TV (and other household smart/connected devices)

The giants:

1. Microsoft - Hardware, OS, office, (skype, )
1. Google - OS, Playstore, Maps, Docs, messaging
1. Apple - Hardware, OS
1. Amazon - Shopping, AWS
1. Facebook - Social Media, What's App, Instagram
1. Twitter
