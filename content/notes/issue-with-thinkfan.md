---
title: "Issue With0 Thinkfan"
date: 2019-04-02T22:54:42+01:00
description: ""
draft: true
---

Had an issue with the thinkfan service loading on boot, getting fail.  Running systemctl status thinkfan reported that the acpi might not be loaded and asked if this was actually a thninkpad or not.

Decided to load the thinkfan towards the end of the service startup using 

```
$ sudo update-rc.d 'thinkfan' defaults 100
```
