---
title: "Colour Management on Kubuntu"
date: 2019-03-08T23:42:38Z
description: ""
draft: true
---
Have installed a new screen, when I first turned it on the white parts of the screen had a yellow tint.

I searched kubuntu for a colour profile manager.  I didn't find anything installed on kubuntu distro nor on plasma desktop.

Amongst other things I came across colord-kde which could be useful in the near future, it's a graphical interface for colord (a system service to manage device colour profiles).

After using my new laptop screen for an hour or so now I've either become accustomed to the colour or it's changed to be more white.  I think it's the latter, maybe a new laptop screen needs a bit of warming up.  I'm happy.  Will check again when booting up from cold tomorrow.

Also, might be useful for the future:
* [color hug](https://hughski.com/colorhug2.html) uk based, open source hardware colour profile manager to be used with colord-kde.
* [xgamma](http://manpages.ubuntu.com/manpages/precise/man1/xgamma.1.html) control the RGB settings of a screens
* [xrandr](http://manpages.ubuntu.com/manpages/precise/man1/xrandr.1.html) also has RGB and brightness settings
