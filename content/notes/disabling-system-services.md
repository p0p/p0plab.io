---
title: "Disabling System Services"
date: 2019-04-02T22:14:10+01:00
description: ""
draft: true
---
We can take a look at what services are starting up on boot in Ubuntu and the times that are taken for them to boot.  Eg on my machine this looks like this:

```
:~$ sudo systemd-analyze
Startup finished in 4.799s (kernel) + 4.614s (userspace) = 9.413s
graphical.target reached after 4.606s in userspace
:~$ sudo systemd-analyze blame
          2.573s snapd.service
          2.467s docker.service
          2.460s dev-sda1.device
          1.805s mpd.service
          1.774s ModemManager.service
          1.635s minidlna.service
          1.457s udisks2.service
          1.260s accounts-daemon.service
          1.161s systemd-rfkill.service
          1.028s apport.service
           957ms networkd-dispatcher.service
           952ms avahi-daemon.service
           950ms NetworkManager.service
           949ms wpa_supplicant.service
           856ms bluetooth.service
           849ms tpacpi.service
           824ms grub-common.service
           812ms thermald.service
           704ms rsyslog.service
           692ms gpu-manager.service
           687ms systemd-logind.service
           603ms apparmor.service
           371ms systemd-journal-flush.service
           292ms lm-sensors.service
           282ms dev-loop0.device
           281ms systemd-resolved.service
           280ms systemd-timesyncd.service
           253ms dev-loop1.device
           247ms dev-loop2.device

```
so there are a number of services I can turn off.  Hopefully nothing wants to use them and will fail, if I get failures here's a ref to remember what I turned off:

```
~$ sudo systemctl disable docker.service
```
I'm using docker for some dev work but not all the time so when I want it I'll start it.

```
~$ sudo systemctl disable docker.service
```
ModemManager is used for WWAN connection using 2G,3G,4G etc - I'm not using a WWAN card or dongle

```
$ system systemctl disable mpd.service
```
mpd is the music player daemon, I need to test if spotify will work without this.

```
$ sudo systemctl disable minidlna.service
```
minidlna is used to send files (photos, videos, etc) to DLNA/UPnP I need to make sure that the printers (and other devices) aren't affected. 

```
$ sudo systemctl disable --now avahi-daemon.socket && sudo systemctl disable --now avahi-daemon.service
```
This is an implementation of Apple's Zeroconf protocol (like bonjour) and assign IP addresses and connect to devices without a DHCP server.  I'm avoiding Apple devices so I probably don't need this. - There was a slight issue with this one and the socket which is used to start up the service if it is required keeps restarting the service. (see this post on it)[https://unix.stackexchange.com/questions/255197/how-to-disable-avahi-daemon-without-uninstalling-it#255204]

```
$ sudo systemctl disable rsyslog.service
```
This utility rsyslog forwards log messages to an IP address.  I'm not planning on remote logging so this can go.

```