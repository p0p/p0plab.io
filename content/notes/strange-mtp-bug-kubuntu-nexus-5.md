---
title: "Strange Mtp Bug Kubuntu Nexus 5"
date: 2019-02-06T01:48:40Z
description: ""
draft: true
---
*very confusing*

* `sudo apt-get install mtp-tools` (this might not have been needed)
* uncomment`user_allow_other` in `/etc/fuse.conf` (I'm uncertain whether this matters)

* plug in USB
* select transfer files on phone
* 2 folders to open plus photos on Kubuntu device notifier
  * 1 folder to parent of this dir ("Google Pixel/Nexus MTP") - call this dirA
    * Files can be written onto dirA
    * Files can not be copied or moved from dirA
  * 1 folder to folders on phone (documents, pictures, etc) - call this dirB
    * Files can not be written onto dirB
    * Files can be copied/moved from dirB

_the behaviour is very flaky - I've had numerous things work and not work in different combinations, there must be some kind of MTP conflict_
