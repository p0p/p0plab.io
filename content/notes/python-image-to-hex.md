---
title: "Python Image to Hex"
date: 2019-01-28T08:58:41Z
description: ""
draft: true
---
I need to convert simple image with text in it to an array of hex values so that I can add new icons to my wacom tablet OLEDs.

The image sizes are 64px by 32px (I need to check this)

The output needs to be in greyscale 8-bit hex (I need to check this).

I can use  [Python PIL](http://effbot.org/imagingbook/concepts.htm) to read dumpImageAsStaticData

* [open an image with eval](https://stackoverflow.com/questions/5749794/pil-check-pixel-that-its-on-with-eval-function) which can send a function to that image to load
* use the [draft function](http://effbot.org/imagingbook/image.htm#tag-Image.Image.draft) passing in the respective mode to get the greyscale image (i believe this is "L")
* the [load function](http://effbot.org/imagingbook/image.htm#tag-Image.Image.load) should have returned an object that can read the value at each pixel using cartesian based coordinates
