---
title: "Monitoring Cpu Temp with lm-sensors"
date: 2019-01-25T21:35:50Z
---
Have used lm-sensors (on Ubuntu 18.10)

`$ sudo apt-get install lm-sensors`

Follow setup, say yes to scans, allow to add findings to `/etc/modules`

`$ sudo /etc/init.d/kmod start`

`$ sensors`

also have `$ sudo apt-get install fancontrol` installed found fan through `$ sudo pwmconfig`but not used

see:

- https://askubuntu.com/questions/53762/how-to-use-lm-sensors
- http://manpages.ubuntu.com/manpages/cosmic/en/man5/sensors.conf.5.html

Currently just realtime in console `$ watch sensors`
Also want to install gnuplot to monitor realtime graphs

- https://stackoverflow.com/questions/44470965/how-can-you-watch-gnuplot-realtime-data-plots-as-a-live-graph-with-automatic-up
- http://hxcaine.com/blog/2013/02/28/running-gnuplot-as-a-live-graph-with-automatic-updates/
