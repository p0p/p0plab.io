+++
title = "Homepage"
author = ["Tom Broughton"]
draft = false
Subtitle = "Writings of a Creative Technologist, Maker, Geek"
Title = "Tom Broughton's Homepage"
+++

I'm Thomas Alan Broughton, a 40 something Creative Technologist,
Designer-Maker, Writer, Student of Cousnelling and Psychotherapy and
full time Geek.  What I write here is mainly for myself so I remember
what I've done (and who I am).  It's partly about a journey of
self-discovery and partly about leaving behind a trace of what I did
whilst here on Planet Earth.

You're welcome to take a look around.  The content is broad and
diverse due to my varied interests and tendency to get fixated on
random but creative projects. It could be chaos and because of this
I've put some effort into structuring my site so that others might
find their way to something that's useful or at least of some
interest.

If I'm still alive then I always welcome constructive feedback and I'm
open to discussions and debate.  I'm also available to work on
projects of interest, facilitate workshops and group activities, coach
or mentor folks to see through their next goal or give a talk on a
relevant topics you come across here on this site.
