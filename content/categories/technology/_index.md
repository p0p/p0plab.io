+++
title = "Technology"
author = ["Tom Broughton"]
date = 2020-03-24T22:49:00+00:00
draft = false
+++

I write about technology as I explore it and make it work for me.
I'm a believer that technology should work for our needs so that it
enhances our lives rather than it dictating how we should live
through it's demands on us.

I find the consumer culture grotesque and aim to use technology for
as long as possible so that I can help to protect the environment
as well as my wallet.

I believe we should own and have control over our data and
recognise that whilst there are limited regulations and
transparency on how our data is collected and used that I need to
consider the implications of what I services and technologies I
use.

I evangelise for Open Source software and hardware for equity and
social justice.  Everyone should have the opportunity to use and
contribute to good tools and services.
