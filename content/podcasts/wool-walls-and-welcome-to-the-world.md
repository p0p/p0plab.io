+++
title = "Wool, Walls and Welcome to the World"
author = ["Tom Broughton"]
draft = false
series_weight = 1
series = "Tomologue"
+++

In this episode I climb a hill and contemplate the stone walls as I
walk past them.  I consider the animals I pass and the wool on the
sheep as well as greeting two newly born lambs on my way.

