+++
title = "Self-Hosted Docker Gitlab, CI and Pages"
author = ["Tom Broughton"]
date = 2020-04-12T16:08:00+01:00
draft = false
avatar = "/images/series/gitlab-logo.png"
+++

Over the past day or so I was trying to help a [friend](https://adamprocter.co.uk/) set up their
Gitlab instance to host [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) and publish content via [Gitlab
CI/CD](https://docs.gitlab.com/ee/ci/).  It turns out it's not that straight forward so I thought I'd
document setting up a self-hosted [Gitlab CE](https://about.gitlab.com/install/ce-or-ee/) using docker to do just
this.

I'll run both Gitlab and the runners that are used for the CI/CD
processed in Docker containers, [Docker is well supported by Gitlab](https://docs.gitlab.com/omnibus/docker/) and
is generally the industry standard now for hosting services I have
decided I'll run Docker locally to test it out.

What I'm doing is a prototype server rather than a production
ready one as there is so much to cover to create a robust and secure
live service though I'll note points to consider for a live
environment as I come across them.  I would however, spend much longer
and much more time automating and hardening the solution for
production.
