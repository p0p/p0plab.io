+++
title = "The Making of THIS Site"
author = ["Tom Broughton"]
date = 2020-03-24T22:49:00+00:00
draft = false
avatar = "/images/series/ox-hugo.png"
+++

I publish this site using [Hugo](https://www.gohugo.io), I write code using [Emacs](https://www.gnu.org/software/emacs/) have
started to do most of my writing using [Org-mode in Emacs](https://orgmode.org/) too, I have
now started to use org-mode to organise and write the content in this
site whilst still publishing via Hugo.  To do this I use [Ox-Hugo an
exporter from org-mode to Hugo.](https://ox-hugo.scripter.co/)

In this short series I explain how I have set the site up and the
publishing process.  I include how my site is laid out and how all the
content is managed in a single org file.
