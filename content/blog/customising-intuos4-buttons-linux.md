---
title: "Customising Intuos4 Buttons Linux"
date: 2019-01-27T22:02:34Z
description: "Adding Wacom intuos4 configuration on linux"
---

I have a second hand intuos4 tablet, it's the large version.  It has 8 buttons each with an OLED screen to display what the button does.  There's a 9th button in the centre and a dial too along with 2 buttons on the pen.  I believe the medium version has the same controls.

On Linux (currently Ubuntu 18.10 running Plasma KDE - Kubuntu) the driver for pressure work great out of the box, there are a few buttons that work too - right click, scroll etc.  However the buttons are more useful if they are configured (and it would be best if the OLEDs could be configured too).

## Options in GUI

I tried a couple of GUI packages but which were in my package browser there's Wacom Table for GNOME desktop and Graphics Tablet for Cinnamon desktop (Linux Mint) neither of which worked on Plasma KDe but if you have either of those desktops then you might want to try them.  I suspect this [wacomtablet](https://github.com/KDE/wacomtablet) to be the package but I can't get working easily, I don't like the warning of littering the system by building it.  Maybe it will be integrated into future Plasma KDE.

Without a GUI you'll need to find a command line tool to make use of the linux drivers.  Most linux distros will have the drives but if your distro doesn't have the drivers then you should go and read about the [linux wacom project](https://linuxwacom.github.io/), installing what you need for your needs (I suspect in most cases it's all the drivers listed).

## Using xsetwacom

In the console you should be able to list out the devices attached:

```
$ xsetwacom list
Wacom Intuos4 8x13 Pad pad              id: 9   type: PAD       
Wacom Intuos4 8x13 Pen stylus           id: 10  type: STYLUS    
Wacom Intuos4 8x13 Pen eraser           id: 16  type: ERASER    
Wacom Intuos4 8x13 Pen cursor           id: 17  type: CURSOR   
```

If yours doesn't list then your tablet might not be supported.

You can read the xsetwacom manual as readout on Ubuntu.  We will make use of the `xsetwacom set` command to set the function of the buttons.  The buttons are from 1 - 3 and then 8 onwards missing out 4 - 7 (assume these addresses in the internals of the intuos 4 are for something else).

It's useful to run the list parameters and modifiers to familiarise yourself with your tablet (to see what's possible to configure etc) eg:

```
$ xsetwacom list parameters "Wacom Intuos4 8x13 Pad pad"
Area             - Valid tablet area in device coordinates.
Button           - X11 event to which the given button should be mapped.
ToolDebugLevel   - Level of debugging trace for individual tools (default is 0 [off]).
TabletDebugLevel - Level of debugging statements applied to shared code paths between all tools associated with the same tablet (default is 0 [off]).
Suppress         - Number of points trimmed (default is 2).
RawSample        - Number of raw data used to filter the points (default is 4).
PressureCurve    - Bezier curve for pressure (default is 0 0 100 100 [linear]).
...
```

The set command will allow you to change the value of something, changing a value of button will change the function.  Worth noting this can be a chain of keystrokes.

```
$ xsetwacom "Wacom Intuos4 8x13 Pad pad" Button 2 "key +ctrl z -ctrl"
$ xsetwacom "Wacom Intuos4 8x13 Pad pad" Button 3 "key +ctrl +shift z -ctrl -shift"
```

Worth noting on Intuos 4 button 1 is the centre button on the pad.

## OLED Displays

The OLED displays are a bit trickier, maybe quite unique to the Intuos 4 (M and L).  The method I'm using has been developed in C by a [Christopher Karg](https://braindump.kargulus.de/?p=321&cpage=1) and whilst it is ageing without much demand for maintenance it still works with a few tweaks - see comments in the post.

I had to install a package `graphicsmagick-libmagick-dev-compat` and add the lines suggested by Jess in the comments.

## Configuring via a script

I want a script to set up my wacom which will configure the buttons for me for when I'm suing [Krita](https://krita.org/en/), I may also want scripts for other programmes (or people).

```
#! /bin/bash

# Device name of the tablet retrieved from xsetwacom
DEVICE="Wacom Intuos4 8x13 Pad pad"

# Path to the tablet config application
LEDCMD=~/Scripts/intuos4-led/src/intuos4-led-config

sudo rmmod wacom
sudo $LEDCMD --button 1 --icon Undo
sudo $LEDCMD --button 2 --icon Redo
sudo $LEDCMD --button 3 --icon ZoomIn
sudo $LEDCMD --button 4 --icon ZoomOut
sudo $LEDCMD --button 5 --icon Save
sudo $LEDCMD --button 6 --icon Alt
sudo $LEDCMD --button 7 --icon Ctrl
sudo $LEDCMD --button 8 --icon ArrowUp
sudo modprobe wacom

sleep 1

xsetwacom --set "$DEVICE" Button 2 "key ctrl z"
xsetwacom --set "$DEVICE" Button 3 "key ctrl shit z"
xsetwacom --set "$DEVICE" Button 8 "key plus"
xsetwacom --set "$DEVICE" Button 9 "key minus"

xsetwacom --set "$DEVICE" Button 1 "key ctrl"

xsetwacom --set "$DEVICE" Button 10  "key ctrl s"
xsetwacom --set "$DEVICE" Button 11 "key alt"
xsetwacom --set "$DEVICE" Button 12 "key ctrl"
xsetwacom --set "$DEVICE" Button 13 "key shift"
```
## Creating Own Labels
There's a script in the icons dir called `text-to-icon.sh` the Christoph wrote to create a simple image from some text.  It takes in 2 values, one for the text and one for the file name (which will be prepended by icon-).

```
#! /bin/bash

#you need to change the following line to a truetype font on your system
TEXTFONT=/usr/share/fonts/truetype/ubuntu/Ubuntu-C.ttf

convert -background black -fill white -size 64x32 -pointsize 14
-gravity center  -font $TEXTFONT
label:"$1" icon-$2.png
```

so the command `$ ./text-icon.sh "Save As..." save-as` will create a small png `icon-save-as.png` which can then be used with something like `sudo ../src/intuos4-led-config --image icon-undo.png --button 1`

## script template

I create different wacom configurations for different applications and I add the icon creation step to ensure they exist. Here's an example for my Krita config.

```
#! /bin/bash

# Device name of the tablet retrieved from xsetwacom
DEVICE="Wacom Intuos4 8x13 Pad pad"

# Path to the tablet config application
ICONSPATH=~/Scripts/intuos4-led/icons
TICMD=$ICONSPATH/text-icon.sh
LEDCMD=~/Scripts/intuos4-led/src/intuos4-led-config

# Create images
#(apart from any converted from acutal image rather than text like arrow up)
$TICMD "Undos" undo
mv icon-undo.png $ICONSPATH
$TICMD "Redo" redo
mv icon-redo.png $ICONSPATH
$TICMD "Swap Colour" swap-colour
mv icon-swap-colour.png $ICONSPATH
$TICMD "Mirror Image" mirror-image
mv icon-mirror-image.png $ICONSPATH
$TICMD "Deselect" deselect
mv icon-deselect.png $ICONSPATH
$TICMD "Eraser Mode" eraser-mode
mv icon-eraser-mode.png $ICONSPATH
$TICMD "Save As..." save-as
mv icon-save-as.png $ICONSPATH

#set the OLEDs - button numbers corresond to OLED position top to bottom
sudo rmmod wacom
sudo $LEDCMD --button 1 --image $ICONSPATH/icon-undo.png
sudo $LEDCMD --button 2 --image $ICONSPATH/icon-redo.png
sudo $LEDCMD --button 3 --image $ICONSPATH/icon-swap-colour.png
sudo $LEDCMD --button 4 --image $ICONSPATH/icon-mirror-image.png
sudo $LEDCMD --button 5 --image $ICONSPATH/icon-arrow-up.png #this is not made on the fly
sudo $LEDCMD --button 6 --image $ICONSPATH/icon-deselect.png
sudo $LEDCMD --button 7 --image $ICONSPATH/icon-eraser-mode.png
sudo $LEDCMD --button 8 --image $ICONSPATH/icon-save-as.png
sudo modprobe wacom

sleep 1

#button numbers for driver do not correspond to button numbers for OLED!
#here they are laid out top to bottom
xsetwacom --set "$DEVICE" Button 2 "key ctrl z"
xsetwacom --set "$DEVICE" Button 3 "key ctrl shift z"
xsetwacom --set "$DEVICE" Button 8 "key x"
xsetwacom --set "$DEVICE" Button 9 "key m"
#my big button in the middle has no LED
xsetwacom --set "$DEVICE" Button 1 "key ctrl"

xsetwacom --set "$DEVICE" Button 10  "key shift"
xsetwacom --set "$DEVICE" Button 11 "key ctrl shift a"
xsetwacom --set "$DEVICE" Button 12 "key e"
xsetwacom --set "$DEVICE" Button 13 "key ctrl shift s"
```

## Issues
I can configure my buttons but I'm having issues with creating new icons for the OLEDs.  I had to:

* install `graphicsmagick-libmagick-dev-compat` as code wouldn't compile with libmagick++-dev
* added the lines `InitializeMagick("/usr/bin/");` to the `WacomIntuos4LED::imageToIcon`
* added a method to the WacomeIntuos4LED class to release the usb and reload it and called it just before exit in intuous-led-config

```
void WacomIntuos4LED::releaseAndReloadWacom(){
	libusb_release_interface(dev_handle, 0);
  libusb_attach_kernel_driver (dev_handle, 0);
}
```

The other issue is that you have to run the commands as root using sudo to change the OLEDs, this is because of the interface to the usb.
