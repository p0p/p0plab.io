---
title: "Ergonomically Mechanical Keyboard"
date: 2019-07-09T21:19:18+01:00
description: ""
resources:
- src: "*/mistel-MD600-RGB.jpg"
  title: "Mistel Barocco with numpad"
  name: mistel
  params:
    link: http://www.mistelkeyboard.com/barocco-rgb-series/
- src: "*/uhk-hero-886.jpg"
  title: "Ultimate Hacking Keyboard"
  name: uhk
  params:
    link: https://ultimatehackingkeyboard.com/
- src: "*/kinesis-advantage2.jpg"
  title: "Kinesis Advantage2"
  name: advantage2
  params:
    link: https://kinesis-ergo.com/shop/advantage2/
- src: "*/freestyle-pro.jpg"
  title: "Kinesis Freestyle Pro"
  name: freestylepro
  params:
    link: https://kinesis-ergo.com/shop/freestyle-pro/  
- src: "*/esrille.jpg"
  title: "Esrille New Keyboard"
  name: esrille
  params:
    link: https://www.esrille.com/keyboard/index.html
- src: "*/ergodox.jpg"
  title: "Ergodox EZ"
  name: ergodox
  params:
    link: https://ergodox-ez.com/
- src: "*/keyboardio.jpg"
  title: "keyboardio"
  name: keyboardio
  params:
    link: https://shop.keyboard.io/
- src: "*/freestylepro-uk-ansi.jpg"
  title: "My Freestyle Pro, UK ANSI layout with Tai Hao Hawaii keys"
  name: freestyletom
- src: "*/koipondkeycaps.jpg"
  title: Koi Pond Keycaps, Jelly Key.
  name: koipond
---

For the past few years, when at my desk, I've been typing on a Microsoft ergonomic keyboard and it's been very good to me.  For the past few months though I've been spending a lot more time at my desk and typing a lot.  Maybe it's because I've modded my laptop to the max and I'm subconsciously looking for a new project but for the past few months I've been pawing over pages on mechanical keyboards.  I thought mostly mechanical keyboards were flashing devices for gamers but things have moved on an many of them are for developers and authors - people who type a lot and want a richer experience from it.  I've become one of those too.

However, I've been put off by 3 things. 1) I didn't know anything about them. 2) I couldn't get access to any of them to try. 3) I like an ergonomic/split keyboard and there aren't many of those.  Oh and a fourth thing - the price... these are serious squids, but then typing is mostly my livelihood so I might as well have a quality tool for my job.

After months of considering the risks of just buying something and wondering how I'd afford it I had a breakthrough.  I had a birthday recently, the one which is where life starts, and my parents asked me what I wanted.  I wanted something that would last a good while and that I could use a lot and remember the gift through using.  It's actually been over 18 months and it dawned on me that a quality keyboard could be that thing.  I think by this time I'd also done enough research to be able to make a semi-educated guess.

## What's a Mechanical Key? ##

I'll be very quick here.  With cheaper keyboards the keys are laid out on top of a rubber membrane with pressure buttons inside that trigger the key press.  In contrast to this the mechanical keyboard has a physical switch per individual key.  The switch is usually contained inside a small box and includes physical elements such as a stem, spring, contacts and a diode.  Different mechanical switches have different properties e.g. the hardness of the spring inside (the firmness), the tactile feel, the travel, and even audible sound such as clicks or not.  Different manufacturers make different qualities and slightly different form factors.  One of the best and most common is Cherry MX - in fact those keys can be found in old IBM and Compaq keyboards from 20 or 30 years ago.  Mechanical switches last longer, can be individually replaced and give a superior user experience.  To literally top this off, each switch can fit different keycaps in different colours, materials and shapes.. Could there be a geekier fascination?

## Different Strokes for different folks ##

Most people have typed on a keyboard and mostly those keyboards are the same with a few country variations and 2 main physical variations - ISO (the big enter key) and ANSI (the slimline enter key) differences.  With mechanical keyboards, and especially with ergonomic ones, things can get very different very quickly.  Those country and standards variations are still there but there are so many more options too such as key types, number of keys (% completeness of board), the caps on the keys, lights, colours, internal programming, and physical design. 

Here are a few photos of some so you can see what I mean.

{{< imglink mistel "£200 without numpad" >}}

{{< imglink  uhk "£400 for base unit with shipping and VAT" >}}

{{< imglink advantage2 "£400 for UK version" >}}

{{< imglink esrille  "A whopping £500" >}}

{{< imglink freestylepro  "£180 shipped to UK" >}}

{{< imglink ergodox  "About £380 for basic unit + VAT" >}}

{{< imglink keyboardio "£400 with shipping to UK" >}}

## What's important to me? ##

* A UK layout, I like my £ on my 3 and my @ next to my little finger.  This has proven difficult to find though. Whilst some of the keyboards allow you to change keycaps only the Kinesis Advantage 2 has a UK specific layout though the Ultimate Hacking Keyboard does come in ISO which is very close if keys are reconfigured.
* Keycaps - I want to be able to configure my keyboard and change a few keys if I need to.  Plus I'd like to be able to change the keys from time to time to change the appearance.  The only keyboard here that that's not possible with is the keyboardio as the keys on it are especially made whereas the others are mostly standard keys.
* Price is still a consideration to me, I can't really justify the expensive ones when I've only the cheap Microsoft keyboard I have (which was £40) to compare it to.


## I have made a decision ##

For the price and the features plus the confidence I get from forums and from their site I've decided to go with the Freestyle Pro.  I have added a tenting option which is extra (as it is with some of the other keyboards I've listed) and I've also added a Kinesis numpad to go with it (which is about £45).  

I've also ordered some keycaps I've been hanging my nose over from Tai Hao.

Here is the result:

{{< imglink freestyletom "Cost with all components about £330">}}

## My Thoughts on the Result ##

I'm really pleased with it, I have been looking for more excuses to type beyond my work, hence this post. I'm still getting used to it but it's far superior to my previous keyboard.  I've been able to re-code some keys and put keys into the UK places.  The only thing is that this is an ANSI layout rather than an ISO meaning there's a key missing but I've mapped that to a spare key really close to where it would be anyway.

I'm able to program this keyboard too, so not only can I remap keys I can create macros, keystrokes.  I've set up a key to launch Firefox and one for terminal too... I've got a compose key (as I'm a Linux user) and I've set out new keycaps on this key and the function key.  I've plans to get more and make some too.  Yep make some, look at this I found - people make and sell artisan keys and they can be so beautiful... I feel another project looming!

{{< imglink koipond >}}

