---
title: "Setting Up Nextcloud as an Alternative to Google Drive"
date: 2019-02-08T12:24:37Z
description: ""
draft: true
---
I'm going to set up Nextcloud on Linode (though these instructions will more or less work on any linux host).  I'm using Linode as an alternative to AWS and I'm going to experiment with Nextcloud as an alternative to Google Drive (using open office files and the Collabora plugin as an alternative to Google docs/sheets/etc).
se
