---
title: "Working on a Beach"
date: 2019-05-14T15:35:12+01:00
description: ""

---
I'm in Portugal after going to the Creative Commons Summit and I've parked a hire car about an hour south of Lisbon, waled sown a river and ended up in a beautiful deserted beach.  I have full reception on 4G and decided to see how remote working on a beach would be.  Sitting on a rick with another rock to my back the gentle lapping of the waves sooth me as I type.  I could certainly get used to this.  Maybe I should look for work I can take away with me and see parts of the world I wouldn't get chance to see whilst earning money to pay for my travels and anything needed back home.  That would be a lovely experiment.
