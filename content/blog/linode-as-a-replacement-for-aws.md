---
title: "Linode as a Replacement for Aws"
date: 2019-02-08T12:26:34Z
description: ""
draft: true
---
I've just AWS for nearly 10 years now and I am continually amazed at the capabilities it offers.  As part of my effort to reduce my dependency on the tech giants I have decided to look at alternatives.  Whilst AWS offers lots of services and platforms, my main use of it is for the compute power (EC2s) and I often use Lightsail too.  They are brilliant products for basic needs and don't cost much to run either - which is annoying as I now need to experiment with alterntives that might not (will not) be as comprehensive.

My basic requirements are:

1. Smaller independent company
1. Compute power
1. Load balancing
1. Block storage
1. Backups/snapshots
1. basic monitoring
1. CLI and SSH
1. Privacy and security
1. UK/EU hosting
1. Straight forward pricing (comparable to AWS)

I took a look around at the various offerings including [Digital Ocean](https://www.digitalocean.com/pricing/) and [Vultr](https://www.vultr.com/) and the company I've decided to trial is [Linode](https://www.linode.com/about) as they have servers in London, a simple/straightforward approach, lots of info to help, plus I was able to chat to their team directly (the only issue I have atm is their team doesn't have many women on it!).

Once you have an account with Linode you can then spin up instances.  The example I'm using with be their Nanode (1GB, 25GB, 1CPU).  When in the [manager](https://manager.linode.com) you can select instances under the _linodes_ tab - select the instance you want to add and the location (for me _London_) and _Add this Linode!_

Next screen gives a basic linode display with status of "_being created_" - time taken to create is not long but the screen didn't refresh for me. After a few seconds I refreshed the browser and the status was "_new_".

You've got bare metal now so it needs to dress up and provision the linode with an OS on mounted disk - do this with the _rebuild_ menu, selecting the linux distro (I'm using debian so my instructions below are specific for that but should be ok for other distros) plus configuring the disk.  Note that disk partitions and encrypting the disk will make it difficult to backup the linode, I'm not sure the options are here for that anyway but worth noting that linode backups aren't block they are file based.

The linode builds but and is powered off by default click _boot_ to start it running.  

A naked OS on server needs a few precautions - first off we'll create a sudo user so ssh into the box as root: `ssh root@server_ip`

Create a new user account `adduser username` enter a secure password for the user and hit enter to move through the other options (setting name, room, etc).  Now add the user to the sudo group `usermod -aG sudo username`.

You can test this by switching users `su - username` and then `sudo whoami`

Exit the ssh session and log back in as the new user.

[Best practice to use ssh keys auth](https://www.linode.com/docs/security/authentication/use-public-key-authentication-with-ssh/).  If you haven't already got one generate a local ssh (RSA) key pair which will typically be saved somewhere like `~/.ssh/id_rsa` - check you don't ahve one though as creating new ones could lock you out of other systems.

`ssh-keygen -b 4096` and enter a secure passphrase when requested (remember this!).

Upload by logging into ssh console `ssh root@server_ip` and creating the locations to store the public key:

```
mkdir ~/.ssh && touch ~/.ssh/authorized_keys
chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys
```

Then from local machine copy over the public key: `scp ~/.ssh/id_rsa.pub root@server_ip:~/.ssh/authorized_keys`

Now exit the ssh session and log back in, your console (or whatever tool you're using) will probably prompt you for your ssh key phassphrase.

Now we'll remove root's ssh access which will limit exposure to brute force attacks and similar risks (including you logging in as root and doing something by accident).  - The only concern I have here is whether any of the backup services from Linode depend on root access so it that becomes an issue I'll have to revert this step or find a work around.

Whilst logged in as your new sudo user edit the ssh config file `vi /etc/ssh/sshd_config`:
* comment out _PermitRootLogin yes_ `#PermitRootLogin yes`
* remove password authentication by turning on `PasswordAuthentication no`
* restrict processes to connect to IPv4 only `AddressFamily inet` (if using IPv6 only used inet6)

Then restart the ssh deamon ` /etc/init.d/sshd restart`

Let's bring our packages up to date: `apt-get update && apt-get upgrade`

And set the time to GMT use `sudo dpkg-reconfigure tzdata` or `sudo timedatectl set-timezone 'Europe/London'`

Now we'll introduce Fail2Ban, an application that bans IP addresses from loggin ginto the server after too many failed attempts `sudo apt-get install fail2ban`

(you can install sendmail and configure fail2ban to email reports but we're not going to do that, the logs will be stored at `sudo apt-get install fail2ban`)

The .conf files are overridden by .local and these will be protected when apt-get updates the package.  The following files can be overridden:

`/etc/fail2ban/fail2ban.conf` - set the logging levels and log locations
`/etc/fail2ban/jail.conf` - set ban times and services to monitor (eg sshd)
`/etc/fail2ban/filter.d/<filtername>.conf` - create regex filters for specific applications that can be referenced in the jail.conf

[A good reference is on the linode site.](https://www.linode.com/docs/security/using-fail2ban-for-security/)

For instance I've created a jail rule for ssh by first copying the file `sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local` then adding `enabled=true` under the `[sshd]` section.

Adding firewall rules - fail2ban should have already added some rules to the iptables.  Another tool to use as well as iptables makes firewall rules a little simpler - ufw `sudo apt-get install ufw`

ufw will work with fail2ban as is out of the box but to check what rules are in place when using both we will need to  `sudo iptables -L -v -n` _and_  `sudo ufw status`.  This can be changed so that [fail2ban makes use of ufw as outlined here](https://askubuntu.com/questions/54771/potential-ufw-and-fail2ban-conflicts).

We'll set the degaults to allow all outgoing and deny all incoming *but first we'll make sure explicit rule allows incoming on port 22 (ssh) so we don't get locked out!*

```
sudo ufw allow ssh
sudo ufw allow 22
sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw enable
```
when we look at what this had done we can see it's set rules for both IPv4 and IPv6
```
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       Anywhere                  
22                         ALLOW       Anywhere                  
22/tcp (v6)                ALLOW       Anywhere (v6)             
22 (v6)                    ALLOW       Anywhere (v6)     
```
As I'm not using IPv6 atm (and don't plan to in the very near future) then I'll remove this by editing `sudo vi /etc/default/ufw` and making sure `IPv6=no`, then disabling and enabling usf again

```
sudo ufw disable
sudo ufw enable
...

sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       Anywhere                  
22                         ALLOW       Anywhere
```

Add hostname `sudo hostnamectl set-hostname example_hostname` and edit the hosts file `/etc/hosts` like the following example:

```
127.0.0.1 localhost.localdomain localhost
203.0.113.10 hostname.example.com hostname
```

Now we can take a look at what processes are listing on ports with


```
udo lsof -i -n
COMMAND PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
sshd    643 root    3u  IPv4  12033      0t0  TCP *:ssh (LISTEN)
```

Also use [nmap](https://nmap.org/book/osdetect-usage.html) to scan the server from external source.
