---
title: "Installing Docker on Ubuntu 18.10"
date: 2019-03-25T20:10:14Z
description: ""
---

## Introduction

We're going to install Docker CE (community edition) on Ubuntu Cosmic 18.10.

This is ideal for startups and smaller applications that don't require enterprise production environments, in the community version we get the container engine, built in orchestration networking and security. For more advanced features the Enterprise Edition is required.

CU will update on different specified release branches which are: stable (latest general release), test (a pre-release version) or Nightly (bleeding edge progress for next major release).

We will use Stable.

* [Read about Docker CE or EE in Docker's docs](https://docs.docker.com/install/overview/)

The steps we take are:

1. uninstall any previous versions of Docker
1. set up the apt-get repos
1. install docker
1. create a docker group for non-root user as there are some security issues (optional recommended)
1. start the docker daemon on boot using systemd (optional)

## Uninstall any previous versions of Docker

```
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```

In my case on fresh Kubuntu install I have none installed.  We'll now update the apt indexes for the next step.

```
$ sudo apt-get update
``` 

## Set up the apt-get repos
We'll add some packages that help to use apt-get repos over https:
```
$ sudo apt-get install transport-https ca-certificates curl gnupg-agent software-properties-common
```

Now we'll add Docker's GPG key to apt

```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

And then we can check we were given the right key by seaching the last 8 chars of the fingerprint-  We should receive ```9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88``` as the response (at this time of writing):

```
$ sudo apt-key fingerprint 0EBFCD88
pub   rsa4096 2017-02-22 [SCEA]
      9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]
```

if all is good it is safe to add the repo.  The ```lsb_release -cs``` command in the following returns the name of the ubuntu distrobution (verion, in my case it's cosmic).  The command that follows is for x86_64/amd64, there are other commands for other architectures such as arm64 - see the docker docs for these if needed.

```
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

```

If we wanted the _test_ or _nightly_ repo we would have replaced _stable_ with either of those with We'll now update the apt indexes with the new repo.

```
$ sudo apt-get update
``` 

## Install Docker CE

```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Test that Docker installed we'll run a hello world image from the docker hub:

```
$ sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete 
Digest: sha256:2557e3c07ed1e38f26e389462d03ed943586f744621577a99efb77324b0fe535
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Create a Docker group for non-root user (some security issues) 

This step is optional but recommended

Docker daemon requires root privilages to run.  There are some key security issues with this and so it's important that:

* *only trusted users control containers and trusted images run within them*;
* A unix socket over 127.0.0.1 is used for the CLI bu if exposing Docker API the make sure it's done over HTTPS from a trusted network, VPN or is via stunnel and that client SSL certs are issued and checked;
* do not start a container where the /host dir is / as all files in the /host dir can be altered

There are lots more security considerations with suggestions for hardening as sub-pages in the [security section of the Docker docs](https://docs.docker.com/v17.09/engine/security/security).

It's always best to run commands as non root users and because we don't want to use sudo for every ```docker``` command we're going to create a user group for docker and add our own user to it.

```
$ sudo groupadd docker && sudo usermod -aG docker $USER
```

then _log out and back in again_ for the new profile to re-evaluate your group memberships.  To verify this worked run:

```
$ docker run hello-world
```

if there's an error it's because this was run as sudo last time in the stap above, you'll need to delete the ```~/.docker/``` file.  Note if for some reson you've any custom settings in there you don't want to loose then you'll need to change owner and group to your current user and give the group read, write and execute permissions of it.

## Start Docker on boot using systemd

This step is optional, on my local machine I do not start docker on boot but on my web servers I do.

As of version 15.04, Vivid Vervet Ubuntu uses ```systemd``` to manage services when the system boots (for older versions see ```upstart```).

```
$ sudo systemctl enable docker
```
[The docker docs has more info on customising Docker daemon through systemd](https://docs.docker.com/config/daemon/systemd/)
