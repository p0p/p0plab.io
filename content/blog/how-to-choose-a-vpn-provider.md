---
title: "How to Choose a VPN Provider"
date: 2019-02-02T22:18:37Z
description: ""
---
Having set up the router to use [encrypted dns 1.1.1.1 and 1.0.0.1 from Cloud Flare](https://1.1.1.1/) I also wanted to route traffic through a VPN to further protect my privacy from my ISP and anything else tracking me via my IP (Google, Amazon, Facebook etc).

This is all part of my effort to remove my footprints and identity from tracking and my dependency on the tech giants - their power is incredible.  The theory here is that using random IP addresses throughout the world will reduce the amount of information sites can use to deduce about me.  Without this I'm assigned an IP from my ISP very infrequently and it's always within the same range so companies easily track my activity over time.   With a VPN I have a tunnel between me and a chosen server anywhere in the world and requests to sites appear as if they are coming from that server making harder for sites I visit to deduce it is me.  Also all my traffic is encrypted between me and the VPN server meaning my ISP can't see any of the information I am transporting to and from sites I visit.  Even when I visit sites with SSL (https) my ISP can still see the requests I'm making: who I'm visiting, what time, pages I visit etc but when connected to a VPN all they see is the server's IP address I'm tunnelling to and nothing else.

[Here's a great little webpage](https://ipx.ac/run) that will tell you about you, it will show you what any website you access currently can know about you and that's without all the built up profile of activity over time, the use of tracking cookies or even having a specific user accounts with them.

Along with these points there are other reasons to use a VPN, features are generally well marketed on their site.  One of the main reason though is to avoid public wifi (train stations, cafes and bars, etc).  Basically your computer (phone, device) is at the mercy of any router you connect to.  They can record/track what you are doing and they often sell that data to whoever. They can also be malicious too - eg. it can route you to where it wants (even spoof sites to trick you), it can trick you with man-in-the-middle attack, or it can simply scan any data you send through it.  Tunnelling into your VPN when connected to public wifi will protect you from this.

There is one catch and that's the VPN company itself - effectively they're able to see all the information my ISP (or public wifi) would normally see and manipulate my traffic in ways I've mentioned.  Selecting a VPN provider is very important.  There are many so I had to do lots of reading about what they offer and I looked at reviews for them and people's views on Reddit and other such places.  These companies should, for instance, protect your privacy first and foremost and so a good starting point is their privacy policies and terms and conditions.  You're looking for ones that won't sell your data, that only take the data they need to make the service function and even ones that don't do any logging of IP addresses, ones which don't ask for any personal details on sign-up and ones that allow payment using bitcoin or similar (even if you don't use bitcoin it suggests they really won't want to identify you).  Of course, these companies _could_ be liars, they _could_ be spying on you, selling your data for political and commercial gain, they _could_ even be handing over your data to the CSA or GCHQ but your ISP is certainly doing that and I've read on many a security advisor's and privacy guru's blog that not using a VPN is much worse for your privacy than actually using one.  I suspect as well that there would be a lot of info contrary to that too if it weren't the case and I haven't come across it.

I've chosen my provider ([NordVPN](https://nordvpn.com/)) for a number of reasons:

1. They claim to be a company (registered) operating out of Panama which means they are not subject to any US or UK legislation meaning they have to hand over personal data if a court requests it (and if they had it);
1. There is a no logs policy which means data of which sites you visit, when and your IP shouldn't be recorded anywhere;
1. They have many servers throughout the world (currently around 5916 servers across 62 countries to connect to);
1. Applications to connect to the VPN are available for all devices and all OSs plus they have command line Linux utility support and browser plug-in for Firefox (or Chrome if you use that);
1. A kill switch option - this option means when you're out in the cafe and the VPN drops your device won't revert to ISP (or Public wifi) leaving you vulnerable to it without you knowing;
1. They seem to be well respected throughout the privacy/security blogs etc, they seem big enough not to want to risk a scandal, they've been independently audited;
1. They support and provide VPN for a lot of not-for-profits (eg Amnesty) and journalists;
1. At the moment there is a very competitive offer for 3 years and 6 simultaneous devices;
1. The site has some good how to guides and blog posts.

Concerns I have:

1. Within their privacy policy they suggest I should be rest assured by the limited amount of information they hold or log about me and my activity though there are parts in the policy that suggest they will use third party data processors to process analytics gathered from the "website and app analytics" - I'm assuming the app analytics are the VPN connection apps I install on devices so I'd like to know what the analytics are and how it is used.  The privacy policy does state that activities are not "monitored, rerecorded, logged, stored or passed to any third party" - sic.  So I've emailed them to clarify what the app analytics are.
1. They have a "[CyberSec](https://nordvpn.com/features/cybersec/)" feature which will block suspicious websites and ads - basically meaning they scan and analyse content/traffic/packets mess with the responses sent back to you. They mention this is optional feature that can be turned on.  I don't like the idea so I won't be turning it on.
1. Slight worries on the fact that the trust model could be abused.  They can direct false news to me, spoof content/sites, obscure real news, intercept my traffic etc... That's quite powerful. They clearly have ties to third parties throughout the world and are going to be bound by Panama law and possibly influenced by the government there, becoming subject to world political (and commercial) factors.
1. NordVPN _could_ be anyone.

For now I will make use of their services and monitor the type of quality I'm receiving, I'll ask them a few questions and see what I make of my concerns over the 30 day money back period.
