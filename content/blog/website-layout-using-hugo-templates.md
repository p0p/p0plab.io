+++
title = "Site Layout with Hugo Templates"
author = ["Tom Broughton"]
date = 2020-03-07T10:49:00+00:00
tags = ["web development"]
categories = ["technology"]
draft = true
series_weight = 3
series = "Making of this Site"
+++

Hugo uses [Go templating system](https://golang.org/pkg/text/template/) to render html and text. These are html
files with addition of variables and functions to access content items
and their parameters.  Using the templates I am building a theme for
my site from scratch.  The theme I'm building is based on a few sites
I've appreciated for their simplicity, ease of navigation and clarity
in reading.  One of these is [Crafting Interpreters, well worth a read
if you're interested in programming languages](https://craftinginterpreters.com/contents.html), I like really like the
layout of this site and the style used and whilst I won't copy it
directly I am using it as a basis for my own design.

<!--more-->

To start off I have created a blank theme using a hugo command
from the root of my hugo project.

```bash
$ hugo new theme structuredme
```

This has created the basic directory structure within
`themes/structuredme` along with the basic default template
files.  After setting up the theme property in the site config
file the site will be rendered using this theme.  In my case at
the moment it is very basic.


## Layout Template Files {#layout-template-files}

The `layouts` directory has a number of nested template files that are
used to render the site.  Starting with `_default/baseof.html` that
contain the `<html>`, `<head>` and `<body>` tags each file is then
included depending on the type of page being rendered.  For example if
the page is a list of items (hugo branch) then the
`_default/list.html` file is used and if it is an actual page of
content (hugo leaf) then `_default/signle.html` is included.  These
comprise of the main sections of the body and are included in
`baseof.html` with `{{ block "main" . }}{{ end }}` - a go function
that determines which file to include depending on the content type
being rendered.  [How Hugo decides what files to include can be
referenced in the hugo docs.](https://gohugo.io/templates/lookup-order/)

Other elements of the site can be included using partials, which are reusable
files containing snippets of html. `{{ partial "header" . }}` and `{{
partial "footer" . }}` will include the `partials/header.html` and
`partials/footer.html` files respectively.


## Ordering Content in Lists {#ordering-content-in-lists}

When viewing content in list pages, such as a section page or the
homepage, I'm using the default ordering of `weight > date`.  I'm not
using any weights at the moment for normal listings of content so it's
the published date that is determining the order of content.

However, for a series I want the content to be ordered in the order I
want them to be read.  By creating a `series_weight` variable in the
front matter I am able to order the content by the value I set if I
also write a template to use that variable.

I can define this variable in my org file within the `PROPERTIES`
drawer for a subheading using  `:EXPORT_HUGO_WEIGHT: :series n` where n is
the order value such as 1, 2 or 3 etc..  Note if more weights are
added then a `+` is needed with the property such as
`:EXPORT_HUGO_WEIGHT+: :tags 123`, ox-hugo then knows to include the
property rather than overwrite it.

Once the `series_weight` value is set it can be then used in the
`themes/structuredme/layouts/series/list.html` template.  The
following code loops through the content for a series, ordered by the
parameter.

```go-html-template { hl_lines=["10"] }
{{ define "main" }}
  <main>
    {{ if or .Title .Content }}
    <div>
	    {{ with .Title }}<h1>{{ . }}</h1>{{ end }}
	    {{ with .Content }}<div>{{ . }}</div>{{ end }}
    </div>
    {{ end }}

    {{ range .Paginator.Pages.ByParam "series_weight" }}
    <!-- I've outputted the series_weight value just to see if it's working -->
    {{ .Params.Series_weight }}
    {{ .Render "Summary" }}
    {{ end }}
    {{ partial "pagination.html" . }}
  </main>
{{ partial "sidebar.html" . }}
{{ end }}
```


## Creating a New Partial {#creating-a-new-partial}

Partials follow a simpler lookup order of files using only the
`layouts/partials` directory.  Any template file can include a
partial using ={{ partial "path/file" . }} where path is the relative
path to the partials directory and file is the file to include.

I've created a partial to display the position of a content item
within a series.  In my site a piece of content can only reside in one
series.  The following source is from
`themes/structuredme/layouts/partials/series/content_position.html`
and uses the current page's parameters and the
`.Site.Taxonomies.series` map to display a snippet of content.

```go-html-template
{{ if .Params.series  }}
<div>
  <p>This is number {{ .Params.series_weight }} of {{ .Site.Taxonomies.series.Count (urlize .Params.series) }} in the series: {{ .Params.series }}</p>
</div>
{{ end }}
```

An example of the output from this is `This is number 3 of 4 in the
series: Making of this Site`

And it can be included within the templates `layouts/series/list.html`
and the `layouts/_default/single.html` with `{{ partial
"series/content_position.html }}` so that it will be rendered in the
site.


## Applying Sass Styles using Hugo Pipes {#applying-sass-styles-using-hugo-pipes}

Hugo Pipes are a processing pipeline on site build.  Variables or
output from functions can be chained together to input into a
subsequent function.  Hugo has a number of functions useful for
processing SASS.  Sass is packaged with Hugo [if the "extended" version
is installed](https://gohugo.io/getting-started/installing/).  For example on linux using snaps:

```bash
$ sudo snap install hugo --channel=extended
```

Within my theme I have an assets directory and I've created css
directory containing a `main.scss`.  Then within my template that has
the `<head>` html tag I include:

```go-html-template { hl_lines=["3"] }
<!-- Styles -->
<!-- Use Hugo Pipes to process the Sass file (convert to CSS, minify, and add a hash) -->
{{ $main_style := resources.Get "css/main.scss" | toCSS | minify | fingerprint }}
<link rel="stylesheet" href="{{ $main_style.Permalink }}"/>
```

This line creates an object with the name `$main_style` that will be a
css file which is minified and has a fingerprint in the filename so I
can bust caches when I change the styles.  This is done by passing the
`main.scss` first to a function `toCSS` then to another function
`minify` then to another function `fingerprint`.  Finally I use the final
objects `Permalink` property to output the correct path to the created
`css` file.
