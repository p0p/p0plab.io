+++
title = "Node Development Environments using Nodeenv"
author = ["Tom Broughton"]
date = 2020-04-04T23:20:00+01:00
tags = ["web development"]
categories = ["technology"]
draft = false
+++

I'm working on a project for Cartoon Network and we are building an
API on AWS which will accept some JSON and route to Lambda scripts
that interface with a MySQL database.  To do this I want to set up
some local components for development and I'm starting with NodeJS to
create both the API gateway and the Lambda scripts.

I want to contain NodeJS as much as possible and be able to switch
versions and manage multiple packages across different projects I
might work on.  I'm going to do this using [Nodeenv a python module to
manage different versions of NodeJS.](https://github.com/ekalinin/nodeenv)

<!--more-->

I already use virtualenv and so even Nodeenv will be contained within
its own python environment although it could be installed globally
too.  I believe this approach of various virtual environments that
aren't accessible until activated could also offer my overall system
some element of security and resource protection.

I'm using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) which is a helper to create python virtual
environments.  I first create a project called `CN` which I will install nodeenv
(and node) into.

{{< highlight bash "hl_lines=1" >}}
[tom@devbox ~]$ mkproject CN
Using base prefix '/usr'
New python executable in /home/tom/.config/virtualenv/CN/bin/python
Installing setuptools, pip, wheel...
done.
virtualenvwrapper.user_scripts creating /home/tom/.config/virtualenv/CN/bin/predeactivate
virtualenvwrapper.user_scripts creating /home/tom/.config/virtualenv/CN/bin/postdeactivate
virtualenvwrapper.user_scripts creating /home/tom/.config/virtualenv/CN/bin/preactivate
virtualenvwrapper.user_scripts creating /home/tom/.config/virtualenv/CN/bin/postactivate
virtualenvwrapper.user_scripts creating /home/tom/.config/virtualenv/CN/bin/get_env_details
Creating /home/tom/Projects/CN
Setting project for CN to /home/tom/Projects/CN
{{< /highlight >}}

The `mkproject` command creates the virtualenv, sets up a project
workspace and then activates it.  Then I install nodeenv into the
environment.

{{< highlight bash "hl_lines=1" >}}
(CN) [tom@devbox CN]$ pip install nodeenv
Collecting nodeenv
  Downloading nodeenv-1.3.5-py2.py3-none-any.whl (21 kB)
Installing collected packages: nodeenv
Successfully installed nodeenv-1.3.5
{{< /highlight >}}

To finish off the node installation in the environment I have various
options, I can run `nodeenv --list` to see all the versions of node I
could install.  [For options see advanced on the github readme for
nodeenv.](https://github.com/ekalinin/nodeenv#advanced) I'm interested in the latest so I'll simply install into the
current virtual environment with:

```bash
(CN) [tom@devbox CN]$ nodeenv -p
 * Install prebuilt node (13.12.0) ..... done.
 * Appending data to /home/tom/.config/virtualenv/CN/bin/activate
 * Appending data to /home/tom/.config/virtualenv/CN/bin/activate.fish
```

I can see the path to my nodejs and the version to give a quick
check.

```bash
(CN) [tom@devbox CN]$ which node && node -v
/home/tom/.config/virtualenv/CN/bin/node
v13.12.0
```
