---
title: "Installing Ubports Ubuntu Touch"
date: 2019-05-03T21:24:23+01:00
description: ""
draft: 
---
I'm on my way to Devon for a wedding and I've been driving for a few hours so my girlfriend has taken over the last hour or so of the journey. Sitting in the passenger seat I have my laptop out and I'm tethered to my Android phone.  I have another phone, a Nexus 5, in my bag that I've got to test out different operating systems.  I decided to install [UBports - Ubuntu Touch](https://ubuntu-touch.io/get-ut "get Ubuntu touch") and just because it was so straight forward I wrote this a blog post on my Hugo gen site in Emacs whilst I did it.

I'm using Kubuntu on my laptop so got the UBports installer via a snap -  there are other flavours for other operating systems and package managers on the link above.

```
$ snap install ubports-installer
```

The installer is really straight forward with minimal effort required from the user:

  1. Plug in the phone (for me a nexus 5)
  1. Select my device from a list - It didn't detected it but that might be because I rooted this phone some time ago (not a prerequisite for installing Ubuntu touch BTW, I had installed Lineage OS)
  1. The installer downloaded the files and then asked me to restart the phone with boot loader option (holding power and volume down)
  1. After the installer pushed the files to the phone it was complete
  1. That was it - success, then on with the settings on the phone itself.
  
note: If you run the installer from the command line ```$ ubports-installer``` it brings up the up the GUI and also echoed messages on what the installer was doing in the terminal emulator which I found useful to watch.

The settings steps felt really slick and straight forward, I didn't have a sim in the phone but I did connect to WiFi, the android hot spot I was using.  It didn't offer to download any updates and perhaps only used the internet to get the right time from the timezone I selected.   One thing I think could be cleaned up at this stage is selecting a timezone location, I had at least 3 Londons to choose from.

Another slightly contradictory thing was the password for the unlock screen, when asked to enter one I tried a password which was only 7 chars long.  It wouldn't let me save it stating that it was too short so I went for the pin option and it only required 4 numbers.  I might raise this with them if nobody else has, I'm not sure why they've put password constraints on when there's a 4 digit pin option.

In total it took me about 25 minutes and that was tethered to a 4G connection on my Android phone.  I'm really impressed at how slick and easy the installation process was and now I'll put my laptop away and explore the Ubuntu Touch UI for the rest of the journey.
