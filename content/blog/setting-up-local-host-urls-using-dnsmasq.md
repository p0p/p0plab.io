+++
title = "Setting up (local)Host URLs Using dnsmasq"
author = ["Tom Broughton"]
date = 2020-04-12T22:10:00+01:00
tags = ["docker"]
categories = ["technology"]
draft = false
series_weight = 1
series = "Self Hosted Gitlab in Docker"
+++

As I explore running Gitlab server and pages in Docker I'll need some
local host urls to use.  I could just add rows into my `/etc/hosts`
file for the host domains but I'd prefer a more elegant solution.  I'm
going to set up local DNS forwarding using dnsmasq so I can host
Gitlab and Gitlab pages on my local machine.

Gitlab will need 2 host domains, one to route to the main Gitlab UI
and the second to route to the published Gitlab pages.  I'm going to
just run these locally for testing purposed and will use
`gitlab.example.lh` and `pages.example.lh` (I'm using `.lh` to denote
localhost) <span name="live_dns">though</span> I'd just use
a live DNS server in production.

<aside name="live_dns">
If I were hosting on a live server accessible on the internet I would
use my live DNS servers to set a host for the main Gitlab UI and wildcard host
for Gitlab Pages.

```bash
gitlab.mysite.com. 1800 IN A 192.0.2.1
*.gitlabpages.mysite.com. 1800 IN A    192.0.2.1
```

</aside>

Typically on linux systems running `systemd` the network is managed
through a service called `NetworkManager` which has a super useful
plugin for `dnsmasq`.  Dnsmasq is a super lightweight DNS forwarder that
runs locally on many distros.  Dnsmasq can be used easily enough
independently of NetworkManger but I'm going to use the plugin for
convenience.

Firstly I make sure dnsmasq is installed on my distrobution, `$man
dnsmasq` brings up the manual so I know it is, if it wasn't I'd
install it through my package manager.

To activate the plugin I add dnsmasq config to NetworkManager at the path
`/etc/NetworkManager/conf.d/dnsmasq.conf` with the following contents:

```bash
[main]
dns=dnsmasq
```

Then to setup the routing of any host url ending with `.lh` I have
created a file in `/etc/NetworkManager/dnsmasq.d/lh.conf` with the
following line:

```bash
address=/lh/127.0.0.1
```

Finally I restart the NetworkManager service using the command `sudo
nmcli general reload` or I could have restarted the NetworkManager
systemd service eg: `sudo systemctl restart NetworkManager`

Now all the host urls I address will forward to my locahost and I test
this with a `ping`.

```bash
$ ping any.domain.lh
PING any.domain.lh (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.026 ms
64 bytes from localhost (127.0.0.1): icmp_seq=2 ttl=64 time=0.078 ms
64 bytes from localhost (127.0.0.1): icmp_seq=3 ttl=64 time=0.095 ms
```
