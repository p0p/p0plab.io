---
title: "Installing Sass on Linux"
date: 2019-09-24T00:42:41+01:00
description: "Sass can be run directly in linux, this is how to install it"

---
I downloaded the most recent release from the [Sass Github releases page](https://github.com/sass/dart-sass/releases "dart sass download") and extracted the files.

```
$ wget -O dart-sass.tar.gz /
    https://github.com/sass/dart-sass/releases/download/1.22.12/dart-sass-1.22.12-linux-x64.tar.gz \
	&& tar xvf dart-sass.tar.gz
```

Then I moved the extracted files to where I wanted them in the file system.

```
$ sudo mv dart-sass /opt/
```

And finally I made a symbolic link to sass from the bin (which my path looks at).

```
$ sudo ln -s /opt/dart-sass/sass /usr/local/bin/
```

To check I have sass installed I ran

```
$ sass --version
1.22.12
```
