+++
title = "Running Gitlab CE in a Docker Container"
author = ["Tom Broughton"]
date = 2020-04-12T23:16:00+01:00
tags = ["docker", "gitlab"]
categories = ["technology"]
draft = true
series_weight = 3
series = "Self Hosted Gitlab in Docker"
+++

I'm exploring [Gitlab](https://gitlab.com) on a local machine using [Docker](https://docs.docker.com/) and the [official
Gitlab CE Docker images](https://docs.gitlab.com/omnibus/docker/).  The following command will create a
container and run it with various settings.  The command is using an
[environment variable](https://en.wikipedia.org/wiki/Environment_variable) to set the root of the path where Gitalab data,
config and log files will be stored on the host system (my local
machine) so first I set the variable.

```bash
export GITLAB_HOME=/srv
```

And then I run the docker command

```bash { linenos=true, linenostart=1 }
sudo docker run --detach \
  --hostname gitlab.example.lh \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/gitlab/config:/etc/gitlab \
  --volume $GITLAB_HOME/gitlab/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest
```

I'll describe what each line of the command is doing:

-   line 1: command to run docker `--detach` flag is to detach from
    the current tty output
-   line 2: sets a hostname that the container which is set within the
    container (eg in /etc/hosts within the container)
-   line 3: maps the `host-port:container-port` in my example port 80 on my
    local machine will map to port 80 within the container
-   line 4: is a container name we can use in subsequent docker
    commands
-   line 5: `restart always` tells docker to keep the container
    running (even if it is stopped) for example if docker daemon is restarted.
-   lines 6-8: maps paths on the host machine to paths inside the
    container `host/path:container/path` in my example above
    /etc/gitlab in the container is acutally using /srv/gitlab/config
    on my local machine - this is so I can easily access or backup
    some of the files and so I can destroy the container and create a
    new one to use the same files if I like.
-   line 9: is the docker image to use

After running the command I can see the container running with the
`docker ps` command:

```bash { hl_lines=["3"] }
$ docker ps
CONTAINER ID        IMAGE                     COMMAND             CREATED             STATUS                             PORTS                                                          NAMES
080eb413b6b6        gitlab/gitlab-ce:latest   "/assets/wrapper"   57 seconds ago      Up 56 seconds (health: starting)   0.0.0.0:22->22/tcp, 0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   gitlab
```

Notice `STATUS` has a time appended with `(health: starting).
Starting up the container takes some time, a minute or two, Gitlab is
quite a large Ruby based application running that depends on other
services such as nginx, redis and postgresql - all of which are
running inside the container. Once the =STATUS` reads a time appended
with `(healthy)` then Gitlab is ready to be used.  I could now access
Gitlab on my local machine by browsing to `http://localhost/`.

In an earlier post I wrote about [Setting up (local)Host URLs Using dnsmasq]({{< relref "setting-up-local-host-urls-using-dnsmasq" >}}) where I configured all hostnames ending in `.lh` to route to
localhost on my machine.  This means I can stick to the convention I have set
in the container and actually browse <http://gitlab.example.lh> where I
am asked to set a password for the `root` user gitlab account.

I can run the commands `docker stop gitalb` and `docker rm gitlab` to
stop and then remove the container when I'm done.
