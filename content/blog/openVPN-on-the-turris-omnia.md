---
title: "OpenVPN on the Turris Omnia"
date: 2019-02-03T20:14:17Z
description: ""
---
The [Turris Omnia](https://omnia.turris.cz/en/) is an open source router running its own flavour of [openWRT](https://openwrt.org/) (Turris Omnia OS), it has two user interfaces ([Foris](https://doc.turris.cz/doc/en/howto/foris) and [Luci](https://github.com/openwrt/luci)) as well as ssh.

I've mentioned [why I want to connect to the internet through VPN](/blog/how-to-choose-a-vpn-provider/) and this post is about how to set up the router to direct all outgoing traffic through the VPN using [openVPN](https://github.com/OpenVPN).

Within the Foris UI of the Turris Omnia there are options under "updates" to automatically keep certain packages up to date. Selecting these packages within this list effectively enables the respective package.  It is possible to ssh into the router and set up OpenVPN but I'll do as much of it through the UI as possible to limit future conflicts with auto updates applied.  Select the OpenVPN package and save changes.


In Foris there should now be _openVPN_ in the main navigation, we will kick-start setting up OpenVPN with the options available here (which is in fact setting up a server.  Select _Configuration Enabled_  (if you don't already have them then Foris will set up certificates for you). Remember this is for a VPN server which thsi post isn't about, we want a client but we'll set that up soon - for now just leave the default options here and click _Apply Configuraion_.  This step can probably be skipped as we'll disable the server later...  The reason I mention to do it is to ensure that the OpenVPN config is set up as Turris Omnia default config (mastered by the Foris process) which will possibly help keep it in the format needed for future updates and compatibility.  If you're interested what this file looks like take  a look at the file it wrote `:~# vi /etc/config/openvpn`.

The main method we will use from here to edit the openVPN config is a plugin for the Luci UI which isn't in the default install of Turris Omnia OS.  SSH into the router and update the package lists `:~# opgk update` and then install the Luci UI `:~# opkg install luci-app-openvpn`

*Now, we are going to set up a VPN client*. In Luci there should now be an _openVPN_ option under the _Services_ menu.  Here are listed all the instances that are listed in the config file.  Note the _server_turris_ will probably be flagged as enabled and started (pid listed).

You should be able to get client VPN config files from your VPN provider, these usually have a .ovpn extension [an example client file for reference is here](https://github.com/OpenVPN/openvpn/blob/master/sample/sample-config-files/client.conf) - the ca, cert, key contents for the servers you connect to may well be embedded within the files you receive, this is fine.  If you have to download these files and reference them in the config then you will need to scp them into your router with something like `scp ca.crt client.crt client.key root@192.168.1.1:/etc/openvpn` which is where all config files, auth files and certs for servers will be stored.

Using the _OVPN configuration file upload_ give the server you're connecting to a unique name which will be listed with the other config entries and upload the file.  Click _Save and Apply_.  Now we need to edit the files, again this could be done using vi in ssh terminal but we can do it through Luci too.  Click _edit_ against the new entry and the contents of the uploaded .opvn file will be in a textarea.  Because we are going to load the client config without the capability of user input we need to enter the username and password the server needs and point the client config to the details (hoping nobody comes across them as they will be store in plain text!).  In the box below is the optional `auth-user-pass` file details: put your VPN username on first line and password on second line. Now make sure that there's a line in the actual config file above to refernece this file: `auth-user-pass name_of_your_auth-user-pass_file`.  Click _save_.

Go back to _overview_ of openVPN in Luci, select the _enabled_ flag and click _start_ on your new entry if it isn't already started.  Wait  a few seconds an it should say _yes ([pid])_ under started.  You may need to _stop_ the _server_turris_ (disable this to stop it starting on startup).  If your new VPN entry won't start then you may have something wrong with the config or your credentials for the VPN server.  You can check this on the _system log_ menu option under the _status_ menu, scroll to the bottom and hit ctrl+r to refresh, - maybe find 'openvpn' if it's lost amongst other log items but your logs shouldn't be that complex.  If that's too messy then ssh into the router and change directory to `/etc/openvpn` here type `openvpn name_of_your_config_file.ovpn` and watch the output. - Look for errors/warnings/clues in the readout and fix the issues.

When the VPN client is started then you'll have a tunnel to the VPN server in the config.  You now have to set up the relevant routing for your network - my example simply routes all outgoing requests from the lan through the VPN.  We can do all of this in Luci (again it could be done in ssh terminal in relevant config files etc but we want to make use of the UI as much as possible).

_Network > Interfaces_ set up a new interface we can call this `VPN_CLIENT` set the protocol to none (_unmanaged_ in Luci) and then select _custom interface_ and call it something like `tun0` (short for tunnel 0). Submit.  This creates the interface which we will direct the traffic.

_Network > Firewall_ add a new _Zone_ with name  `vpnclient`, input _reject_, ouput _accept_, forward _reject_.  Enable _masquerading_ and _MSS Clamping_ and select _VPN_CLIENT_ as the covered networks (as an aside: this will make use of the routers DNS settings, I have these currently set to Cloud Flare 1.1.1.1 and 1.0.0.1 ).  Click _save_.

Under the same settings page under _zones_ edit the _lan_, scroll down to the Inter-Zone Forwarding section and select _vpnclient_ under _Allow forard to destination zones_ and make sure this is the only option set in this list.  now click _save and apply_.

At this point you have either lost your internet connection from your lan (your router may still be connected) or you have successfully set up the VPN.  Use [this tool](https://ipx.ac/run) to check your browser details and your IP - the important part is ISP - it shouldn't be your ISP.  If you don't have connection or you still have your ISP listed then check over the set up, check the logs, try again.

You might want to back up your router at this stage with [Schnapps](https://doc.turris.cz/doc/en/howto/schnapps).
