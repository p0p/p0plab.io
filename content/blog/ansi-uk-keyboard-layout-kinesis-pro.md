+++
title = "ANSI UK Keyboard Layout on Kinesis Pro"
author = ["Tom Broughton"]
date = 2020-03-04T12:28:00+00:00
tags = ["mechanical keyboards"]
categories = ["technology"]
draft = false
+++

I have a [Kinesis Freestyle Pro](https://kinesis-ergo.com/support/freestyle-pro/) but it came with a US layout for the
keyboard and I wanted a UK one. The issue I've had is that not all the
keys of an ISO UK keyboard fit/map on the standard ANSI layout, for
example the # and ~ on the ISO sit next to the enter key but there is
no key for this on the ANSI layout and I use a UK layout (eg. the £ is
on the 3 key in the number row). This means that I can't access the
missing keys when I use a UK keyboard layout on my OS and I don't want
to use a US layout because my laptop keyboard which I often use is UK.

<!--more-->

The way I've managed this is to use the [compose key that is a trigger
for accessing precomposed characters and symbols](https://en.wikipedia.org/wiki/Compose_key) along with remapping
keys and creating macros on the kinesis keyboard.


## Using the Compose Key {#using-the-compose-key}

So holding down `compose` and `++` will result in `#` - I
can enable the compose key in my os, create a macro on the kinesis
keyboard and map that macro to a key.

[Here's the guide to using the compose key in KDE plasma.](https://userbase.kde.org/Tutorials/ComposeKey) I've enabled
it and added to my dotfiles manager the `~/.config/kxkbrc` file which
is the keyboard config for Plasma just so I can easily restore if I
reinstall/build a new machine for this keyboard. I've enabled the
compose key to be the `scroll lock` key.


## Macros and Remapping on Kinesis {#macros-and-remapping-on-kinesis}

On the kinesis I press `SmartSet + F8` which adds allows me to access
the keyboard as an external drive and in the file `layouts/layout1.txt`
I've created a series of key mapped macros such as:

```text
    {hk6}>{s3}{-scrlk}{-lshft}{=}{=}{+lshft}{+scrlk}
```

Which has mapped a sequence of key strokes to the key hk6 (hk1 to hk10
are special keys on the kinesis, spare for such mappings. In some cases
I've mapped ordinary keys so they are no longer the ANSI ones and are
ISO ones instead).

Here's the full set:

```text
    {\}>{s3}{-scrlk}{/}{/}{+scrlk}
    {lshft}{\}>{s3}{-scrlk}{v}{l}{+scrlk}
    {rshft}{\}>{s3}{-scrlk}{v}{l}{+scrlk}
    {hk6}>{s3}{-scrlk}{-lshft}{=}{=}{+lshft}{+scrlk}
    {lshft}{hk6}>{s3}{-scrlk}{lspc}{hyph}{+scrlk}
    {rshft}{hk6}>{s3}{-scrlk}{lspc}{hyph}{+scrlk}
```


## Extending this Functionality {#extending-this-functionality}

Further to this I can also create some shortcut keys in kde plasma,
for example I have configured various shortcut keys such as `META +
i` to open firefox and `META + k` to open konsole. Then I've mapped
these key strokes to single keys on the kinesis.

```text
    {hk1}>{x1}{s5}{-lwin}{i}{+lwin}
    {hk2}>{x1}{s5}{-lwin}{k}{+lwin}
    {hk3}>{x1}{s5}{-lalt}{lspc}{+lalt}
    {hk3}>{x1}{s5}{-lctrl}{a}{+lctrl}
    {hk4}>{x1}{s5}{-lctrl}{z}{+lctrl}
    {hk5}>{x1}{s5}{-lctrl}{x}{+lctrl}
    {hk7}>{x1}{s5}{-lctrl}{c}{+lctrl}
    {hk8}>{x1}{s5}{-lctrl}{v}{+lctrl}
```

The KDE config file for keyboard shortcuts
`~/.config/kglobalshortcutsrc` lookes quite complex, I have put this
into my dotfiles manager but I should be careful with it in future
plasma installs.
