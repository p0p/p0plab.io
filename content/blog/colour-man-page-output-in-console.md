---
title: "Colour Man Page Output in Console"
date: 2019-09-22T19:05:35+01:00
description: "How to set colours for output of man pages in the console"
---
On linux when you want to know how to use an application you would
typically check a man (manual) page using the command ```man
<app/command>``` e.g.

{{< highlight sh >}}
$ man ls
{{< /highlight >}}

The output is going to be text which, depending on your
console/terminal emulator's theming, will be plain white text.  But
the man pages are coded in such a way that you can also output this
text coloured.

Arch Linux wiki show us how to colour the text by edition your local
profile[^1]. I'm using bash but the wiki has instructions for fish
too.  In the ```~/.bashrc``` file enter the following function which
will be run for ```man```.

{{< highlight sh >}}
# enable colour support for man pages
man() {
  export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
  export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
  export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
  export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
  export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
  export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
  export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
  export GROFF_NO_SGR=1                  # for konsole and gnome-terminal
  command man "$@"
}
{{< /highlight >}}

The last line of that function runs the actual man command and the
preceding lines set the colour etc.

Then just load the profile.

{{< highlight bash >}}
$ source ~/.bashrc
{{< /highlight >}}

And you will get coloured output whenever you read a man page in the terminal.

[^1]: [Read the guide on the Arch Linux wiki](https://wiki.archlinux.org/index.php/Color_output_in_console#man "instructions on how to colour output in linux console")
