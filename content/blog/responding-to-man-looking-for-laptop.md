---
title: "Responding to Man Looking for Laptop"
date: 2019-04-18T17:24:31+01:00
description: ""
---

I recently discovered [Hacker Public
Radio](https://hackerpublicradio.org/ "Go to HP") and I really like
listening to the podcasts people have made.  One podcast was just a
bloke talking about what he wanted from a laptop.  It struck me I had
gone through I similar exercise lately so I decided to leave a comment.

[The podcast I was responding to](https://hackerpublicradio.org/eps.php?id=2777 "listen to the original podcast")

The comments system wouldn't accept my comment - it gave me a 412
Precondition not met (which wasn't great) so I'm pasting my comment
here as it took me a bit of time to write (though you wouldn't believe
it when you read it)... There's a word limit in the comments field on
that site and I kept reaching it and having to re-write it, of course
I had a lot more to say but this is what I ended up with:

<strong>
I left my perm job and had to hand in my macbook...  I had grown a
tired of Apple, the quality of their products has really dropped,
their prices have increased. in my search I listed almost the same
requirements as you - a regurgitation of the best features I'd read
about.

I got a Surface Book 2 which I instantly loved and hated at the same
time. Beautiful machine: using the keyboard and the screen with the MS
pen is very sleek and comfortable.  Windows: through the setup I found
myself in a tech giant war between MS, Apple and Google... MS OS on an
MS machine - It had to go.

I ended up re-evaluating my requirements.  I've ended up with a
machine I love, it happens to be second hand off eBay and quite a few
years old.  Questions I asked myself:

1. Do you need a quad core processor?  Laptops haven't been quad core
   for the smaller laptops up until the 8th gen so do you need a quad
   core 8th gen or will older i7 do?
  * https://en.wikipedia.org/wiki/List_of_Intel_Core_i7_microprocessors#Mobile_processors
  * https://www.notebookcheck.net/Mobile-Processors-Benchmark-List.2436.0.html

2. Do you need 16GB ram? I run numerous virtual machines and do some
   complex compiling - it's fine for me (in fact my macbook had 8GB
   and that was OK usually too). I bought 8GB machine and haven't had
   any issues, I can always upgrade the ram if I need to.

3. Screen - I don't understand QHD on laptops: FHD (1920x1080) is
   perfect and laptops over the past 8 years or so sport these... even
   my TV at 32" that I watch HD bluray films on is this resolution so
   it's going to be really good on a 14" screen! - QHD screen is silly
   (unless you're a video production editor).

How about one of your best choices but slightly older models?  I see
very good machines with warranty on eBay for £200 - £500 ... i7, 8GB,
fhd eg. 3rd gen X1.  Or if you do want high spec look on eBay for T480
or E490 for under £1k.  if you don't like it sell it again.  Also
reusing older machines much better for environment! :-)

</strong>
