---
title: "Crafting the Solarizish Theme"
date: 2019-10-03T14:16:39+01:00
description: ""
draft: true
---
I write this blog in Hugo and I created the theme the theme it uses
when I was inspired by a few things.

1. I liked the layout and readability of [Gregory J Stein's website,
   Caches to Caches](http://cachestocaches.com/) which led me to want
   to design my site.
2. I enjoy dark and light modes for reading at the moment and I
   particularly like [Ethan Schoonover's Solarized colour
   scheme](https://ethanschoonover.com/solarized/) that has been
   designed specifically for reading.  There's a really useful SASS
   snippet I've used on that page I link to.
3. Have you got a favourite text editor? Mine is
   [Emacs](https://www.gnu.org/software/emacs/) - well there's so much
   more to it than just an editor, after 20 years of typing on almost
   a daily basis I've discovered my perfect productivity environment.
   So I'm trying to echo some of the simple and functional principles
   of Emacs, using separate areas for clear purposes that do not
   distract from any of the content.
4. I don't want to use any proprietary licenses, even the fonts I'm
   using are open source, inspired by the Computer Modernised LaTeX
   font that I use when publishing articles as PDFs on this site I'm
   mostly using [CMU fonts available from Font
   Library](https://fontlibrary.org/en/search?query=cmu).
5. I like hexagons, they're so much more pleasing than rectangles and
   circles getting the best of both a tessellating shape and a
   spherical appearance.  [This article on CSS hexagonal grids by
   Codin](https://www.codesmite.com/article/how-to-create-pure-css-hexagonal-grids)
   allows me to render recent posts in a visually pleasing way.
6. Initially to create different shapes (ending up with a hexagon) I
   used [Bennett Feely's really useful clip-path
   tool](https://bennettfeely.com/clippy).
7. I [forked the m10c Hugo
   theme](https://github.com/vaga/hugo-theme-m10c) as a base for my
   theme as it offered me the basic layout I was looking for. Though I
   think by the end of it I will have a very different theme.

