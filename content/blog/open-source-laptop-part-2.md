---
title: "Open Source Laptop Part 2"
date: 2019-02-07T11:54:20Z
description: ""
---

This is an update of my selecting an open source laptop.

Previously I noted that System76 might be a good option, however they're based in the US and the return to base warrenty might not be of much use to me due to cost of shipping etc.

I mentioned that there were no UK retailers but I was wrong, there are - and there's something interesting in that they mostly sell the same machine as the System76.  I haven't read all the information on the System76 website but I was under the impression that the machines were manufactured by the company - looks like I was wrong.  I'm guessing they're whitelabel machines, probably from China that can be customised easily.  

Similar, if not better deals:

* [Juno Computers - Jupiter 14](https://junocomputers.com/store/jupiter-14) - limited component options
* [Station x](https://stationx.rocks/collections/laptops/products/spitfire) - has a high price tag once maxed out on spec compared to Entroware
* [Entroware Apollo](https://www.entroware.com/store/apollo) - can be maxed out with NVMe memory, 32GB 2400 DDR4, etc and reasonable price
* [Star Labtop](https://starlabs.systems/pages/star-labtop) - different body, decent spec at a good price, wonder about usb-c and dock compatibility and whether the 8GB ram can be upgraded.

I have concerns on:

* build quality
* battery life
* wonder what the bios is (and whether it could be libreboot)
* can the usb-c run a dock including charge, monitor, etc

- These are not really open source machines, more open source ready machines (ie. tested and supplied with linux)

Also, keeping with the thinkpad hack idea there is a company in the UK who specialise up repurposing thinkpads called [Ministry of Freedom](https://minifree.org/) - they might be able to help me if I get stuck.

Alternative is to either take a risk with one of these machines (probably Entroware) or go with another high spec machine (thinkpad or similar) requiring minimal modding that can be flashed with libreboot/coreboot to open it up.
