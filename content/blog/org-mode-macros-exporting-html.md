+++
title = "Defining Org-Mode Macros to Export Arbitrary HTML"
author = ["Tom Broughton"]
date = 2020-03-09T17:53:00+00:00
tags = ["web development"]
categories = ["technology"]
draft = true
series_weight = 5
series = "Making of this Site"
+++

Ox-Hugo exports markdown in Hugo from Org content (in Emacs).  The
creators have been stayed true to org-mode features mapping them to
hugo and markdown in the export but there are some hugo features that
don't map such as including arbitrary HTML or user defined Hugo
shortcodes.

In my theme I have `<aside>` blocks of content that at full width
viewport will appear on the right against the main content, they are
positioned alongside `<span>` tags with the same `name` property and
will have some element styles set with javascript to position them
correctly.

Of course there are no export options for this bespoke requirement in
ox-hugo so I have written my own, very simply, using [Org Macros](https://orgmode.org/manual/Macro-Replacement.html).  By
doing this I can define a rule for when the content is exported as
HTML and anther for when it is exported as something else like LaTEX.

<!--more-->

What I want is to be able to export something along the lines of the
following HTML, notice the `<span>` tag in the first paragraph
followed by the `<aside>` content, notice the `name` attributes match:

```html
<p>Along with this paragraph I want to be able to show some extra
content that will go <span name="accompany">alongside</span> whatever it is I
have written here.</p>
<aside name="accompany">
<p>This content will sit alongside the main content and some js will position
it depending on matching the <code>name</code> attrcibute.</p>
</aside>
```

To export the `<span>` tag I have defined the following org macro
replacement which will run on export.  Therefore `{{{span(accompany,
alongside}}}` will export as `<span name="accompany">alongside</span>`.

```org
#+MACRO: span @@html:<span name="$1">$2</span>@@ @@LATEX: $2 @@
```

My aim is to keep the content as readable as possible and as
exportable to other formats as possible such as pdf via LaTeX.  In the
above example I export the second parameter for LaTeX which is the
content.  An alternative way would have to have a pair of macros, one
to open the `<span>` along with a name parameter, and one to close it
with the content in between, just as it is with html.  I may change to
this way of doing things, I'll see how I get on with it for a while as
it is.

Next I have to export the `<aside>` block with a `name` attribute.
Even though it isn't documented [there is a way to export a block of
html with a name attribute in ox-hugo](https://github.com/kaushalmodi/ox-hugo/commit/a2e9396771c8a7ce13c19e20226b1768335aad6c#diff-6fa190cf16cc1fe9ba0fa6673ddb5970R2657).  So to output the above
`<aside>` content I write this in my content

```org
#+NAME: accompany
#+BEGIN_aside
This content will sit alongside the main content and some js will position
it depending on matching the =name= attribute.
#+END_aside
```

However, I've [found an issue with the exporter](https://github.com/kaushalmodi/ox-hugo/issues/338) and I don't like how
this approach reads in the org file, nor do I like how it exports in
LaTeX.  Therefore I've created a macro pair to create the HTML tags
and the content will go in between:

```org
#+MACRO: aside_begin @@html:<aside name="$1">
#+MACRO: aside_end @@html:</aside>
```

I'VE Defined these as only within scope of HTML so they won't export
to LaTeX and I can put up with the macros within the org content.

Putting this all together the following org content:

```org
Along with this paragraph I want to be able to show some extra
content that will go {{{span(accompany, alongside)}}} whatever it is I
have written here.

{{{aside_begin(accompany)}}}
This content will sit alongside the main content and some js will position
it depending on matching the =name= attribute.
{{{aside_end}}}
```

Will output the following HTML:

Along with this paragraph I want to be able to show some extra
content that will go <span name="accompany">alongside</span>whatever it is I
have written here.

<aside name="accompany">
This content will sit alongside the main content and some js will position
it depending on matching the `name` attribute.
</aside>
