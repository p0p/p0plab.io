---
title: "Constraining Drawing Tablet to Single Screen"
date: 2019-03-06T21:06:08Z
description: ""
---
I have a drawing tablet (wacom intuos4) which I use on my laptop, when I'm at my desk I plug into a 24" monitor and extend the desktop (Kubuntu/Plasma) to the right. When I use my drawing tablet the drawing boundaries are aligned to the entire desktop stretched over two screens.  I want to be able to restrict the tablet drawing area to just a single screen, the monitor.

I'm using linux (Kubuntu) so I shall map a device to a screen.

First we need to know what our displays are and our inputs are, we can get them from ```xinput --list```

```
$ xinput --list
⎡ Virtual core pointer                          id=2    [master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer                id=4    [slave  pointer  (2)]
⎜   ↳ MOSART Semi. 2.4G Wireless Mouse Mouse    id=9    [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pen stylus             id=11   [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pad pad                id=12   [slave  pointer  (2)]
⎜   ↳ Microsoft Natural® Ergonomic Keyboard 4000        id=14   [slave  pointer  (2)]
⎜   ↳ SynPS/2 Synaptics TouchPad                id=16   [slave  pointer  (2)]
⎜   ↳ TPPS/2 IBM TrackPoint                     id=17   [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pen eraser             id=19   [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pen cursor             id=20   [slave  pointer  (2)]
⎣ Virtual core keyboard                         id=3    [master keyboard (2)]
    ↳ Virtual core XTEST keyboard               id=5    [slave  keyboard (3)]
    ↳ Power Button                              id=6    [slave  keyboard (3)]
    ↳ Video Bus                                 id=7    [slave  keyboard (3)]
    ↳ Sleep Button                              id=8    [slave  keyboard (3)]
    ↳ Integrated Camera: Integrated C           id=10   [slave  keyboard (3)]
    ↳ Microsoft Natural® Ergonomic Keyboard 4000        id=13   [slave  keyboard (3)]
    ↳ AT Translated Set 2 keyboard              id=15   [slave  keyboard (3)]
    ↳ ThinkPad Extra Buttons                    id=18   [slave  keyboard (3)]
    ↳ Microsoft Natural® Ergonomic Keyboard 4000        id=21   [slave  keyboard (3)]
    ↳ 40:EF:4C:96:24:94                         id=22   [slave  keyboard (3)]

```

We're after the ids of the wacom devices in this case - to be sure we can run:

```
$ xinput | grep -i Wacom
⎜   ↳ Wacom Intuos4 8x13 Pen stylus             id=11   [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pad pad                id=12   [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pen eraser             id=19   [slave  pointer  (2)]
⎜   ↳ Wacom Intuos4 8x13 Pen cursor             id=20   [slave  pointer  (2)]
```

then we need to find the id of the display device we are going to map to

```
$ xrandr
Screen 0: minimum 320 x 200, current 3926 x 1848, maximum 8192 x 8192
LVDS-1 connected primary 1366x768+0+1080 (normal left inverted right x axis y axis) 310mm x 174mm
   1366x768      59.98*+
   1360x768      59.80    59.96  
   1280x720      60.00    59.99    59.86    59.74  
   1024x768      60.04    60.00  
   960x720       60.00  
   928x696       60.05  
   896x672       60.01  
   1024x576      59.95    59.96    59.90    59.82  
VGA-1 disconnected (normal left inverted right x axis y axis)
HDMI-1 disconnected (normal left inverted right x axis y axis)
DP-1 disconnected (normal left inverted right x axis y axis)
HDMI-2 connected 1920x1080+1366+0 (normal left inverted right x axis y axis) 527mm x 296mm
   1920x1200     59.95  
   1920x1080     60.00*   60.00    50.00    59.94  
   1920x1080i    60.00    50.00    59.94  
   1680x1050     59.88  
   1600x900      60.00  
   1280x1024     60.02  
   1440x900      59.90  
   1280x720      60.00    60.00    50.00    59.94  
   1024x768      60.00  
HDMI-3 disconnected (normal left inverted right x axis y axis)
DP-2 disconnected (normal left inverted right x axis y axis)
DP-3 disconnected (normal left inverted right x axis y axis)
```

Now I'm going to map input devices 11, 12, 19 and 20 to display device HDMI-2

```
$ xinput map-to-output 11 HDMI-2
$ xinput map-to-output 12 HDMI-2
$ xinput map-to-output 19 HDMI-2
$ xinput map-to-output 20 HDMI-2
```

## slightly alternative method
If you have ```xsetwacom``` installed then the alternative route is to use ```xsetwacom set <input name> "MapToOutput" <display name>```

eg:
```
$ xsetwacom set "Wacom Intuos4 8x13 Pen stylus" "MapToOutput" HDMI-2
$ xsetwacom set "Wacom Intuos4 8x13 Pad pad" "MapToOutput" HDMI-2
$ xsetwacom set "Wacom Intuos4 8x13 Pen eraser" "MapToOutput" HDMI-2
$ xsetwacom set "Wacom Intuos4 8x13 Pen cursor" "MapToOutput" HDMI-2
```
