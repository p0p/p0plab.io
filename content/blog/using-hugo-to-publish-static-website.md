+++
title = "Using Hugo to Publish This Site"
author = ["Tom Broughton"]
date = 2020-03-04T19:31:00+00:00
tags = ["web development"]
categories = ["technology"]
draft = true
series_weight = 1
series = "Making of this Site"
+++

I built this city from rock 'n' roll, well actually I built this site
from Hugo a static site management tool written in Go.  I've been
using it for a while and have been publishing my pages from it by
writing them in markdown but I have learnt to love Org-mode in Emacs
and to appreciate the control and productivity it offers me.  And
like most things with Emacs there's a tool to manage hugo sites in
org-mode using an interpreter and export tool called ox-hugo.

Hugo uses markdown in a content directory along with a theme,
templates and configuration files to publish static web pages.  Static
web pages can be hosted from almost any computer/server without need
for running specific environments, they're fast and they're safe and
whilst there's no need to do anything dynamic there's no need to run
anything further to serve my website.

<!--more-->

And whilst I'm converting my raw content from markdown into org I'm
also restructuring my site with a new content layout using tags etc, a
new url schema and a new theme, built from the ground up.  It's a
total rebuild.  I'll forget what I do so I'm going to capture it here
in this series.
