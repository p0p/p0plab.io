---
title: "Raspberry Pi Zero Camera"
date: 2019-02-12T18:37:06Z
description: ""
draft: true
---
We're going to make a timelapse camera out of a raspberry pi zero w.

This will form the basis of further projects with the camera such as a wildlife camera trap or a stop motion camera.

I am using a linux (ubuntu) laptop so these instructions are linux specific but it should be more or less the same on any operating system.

## What you need

* Raspberry pi zero w
* Raspberry pi camera module with rpi zero cable
* Micro SD card (8GB) with Raspbian installed - follow the [guidelines here](https://www.raspberrypi.org/documentation/installation/installing-images/) using an [downloaded image from here](https://www.raspberrypi.org/downloads/raspbian/) that has the desktop version.
* 5V power (usb micro b)
* A computer (laptop or similar) OR monitor, keyboard, etc for the pi


## Connecting remotely to your new pi
