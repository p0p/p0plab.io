---
title: "Hot Chocolate Sweet Spot"
date: 2018-01-07T01:23:58Z
description: "I'm on a search for the solutions that align my values, solve problems and are attractive to those with an appetite to scale.  These are the hot chocolate sweet spots."
---
I'm on a search for what I'm calling my _Hot Chocolate Sweet Spot_: a melting pot of personal values, real world problems and desirable solutions which benefit us as humans rather than purely financial or political gain.  The idea is to seek out the intersection where human centric values meets with real world needs to address social justice and inequality, and where the solutions/products/services are so attractive they are readily adopted by society and the people they are meant to serve.

I call it my Hot Chocolate Sweet Spot because of the way Quakers, hundreds of years ago, addressed problems of the day through their values of equality and honesty.  Outlawed as free thinking radicals, Quakers found themselves subject to hostile conditions in day-to-day 17th century Britain.  Dealing with adversity and uncertainty led them to creating some of the most successful businesses in the world [1].  By applying their values Quakers not only approached socio-economic problems in ways that would make the world a better place they did so in a way that was desired by others - their alternative products and services meeting the needs of humans rather than systems.

Cocoa was sold by Quaker grocers and quickly became a desirable drink by people in local tea shops. Quakers recognised this desirable drink as a viable alternative to alcohol.  The chocolaty story of success goes beyond this blog post [2] and sees the Quaker families of Rowntree’s, Cadbury’s and Fry’s dominating the confectionary industry. These organisations were not driven to make huge piles of money, but to generate sustainable funds for their workers to live adequately and contribute to the welfare of others.  Their success was a result of applying their values to a problem of the day in a way which many people desired.

As it was in those days, today's world is still driven by commercial greed and political power.  We live in a time of uncertainty, a time when change can and does happen at an extraordinary pace. We are often passengers being carried along by the tide of capitalist machines solely with their own self-interests as the driving force.  These machines are shaping the world around us and it often not to benefit humanity or the world we live in. Do we challenge or consider the alternatives as Quakers did all that time ago?  Can we change the tilt the balance and strive for a world shaped by human centric values, equality and justice?

If I can apply my values to problems existential problems to humanity in ways that are desired by others then I will have found my hot chocolate sweet spot.

[1] wiki pedia quaker businesses
[2] (_see "Chocolate Wars" by Deborah Cadbury_)
