---
title: "Leaving Las Gmail (Selecting an Alternative Email Provider)"
date: 2019-01-29T09:28:57Z
description: ""
---
As part of my effort to reduce the dependency I have on just a few tech giants and protect my privacy more (avoiding the _free_ services that actually cost a lot in access to your personal data) I need to find a new email provider.  I was going to title this "switching email providers" but I thought I'd be more specific as I'm only really using gmail, and I'm a heavy user too. This is not going to be easy.

My high-level requirements:

* Privacy Policy - clear and non-invasive stating they don't sell data to third parties, location of registered company/servers should be in a physical location (country) where privacy laws offer higher degree of protection (not UK or US);
* PGP encryption - true end-to-end encryption requires this, security is a must of course but the more security there is the more privacy and control is inherited;
* IMAP and SMTP - works well on desktop client (linux eg. KMail or Thunderbird) and phone;
* Includes calendar views - I want to be able to sync into calendar apps or view online;
* Own domain

Whilst going through this process I'm reminded how I felt years ago when setting up email servers and accounts on my own dedicated servers an various suppliers.  I'm sure this technology should be in decline by now in preference for something a bit more secure and light-weight.  There's always the smell of tainted tin running the email services and looking around at alternative email providers to Google I can see a lot hasn't changed in the past 20 years - I'm confronted by a barrage of cheap and nasty options and get I get a slight dirty feel as I trudge through numerous marketing sites.

One thing that has changed is the emphasis on privacy, embodied by an upsurge in start-ups (maybe they are just old hats putting on a new look vaneer) who are based outside of the US and UK offering homespun, hipster, knit_your_own_email, open source, secure email services built to address privacy scandals that have hit the news headlines in previous years.  I'm going to go with one of these though I need to choose carefully.

Here are a few of the better ones I picked out of the one's I took a look at:

[Tutanota](https://tutanota.com/)

* Own domain: yes with aliases
* Location: Germany
* *Calendar: No*
* Privacy: No ads,  No ip loging No tracking, No selling data ,Anon signup
* Security: TLS/SSL, *(end-to-end bespoke not PGP)*, 2FA signin, Mailbox encrypted at rest
* *IMAP/SMTP: No*
* notes: Browser apps wrapped into desktop app and andriod app.  *Info/FAQs on site a bit patchy*
* 12 EUR (10GB) pa

[Posteo](https://posteo.de/en)

* *Own domain: NO*
* Location: Germany
* Calendars: Yes  
* Privacy:	Anon sign-up and payment, strips IP addresses, No tracking or selling data
* Security: Uses TLS (centralised cert issuer), 2FA,  s/MIME or OpenPGP if avail for inbound	only
* IMAP/SMTP: Yes
* Notes: JS browser based app. Feel smaller company with good work culture. Green energy 100%, support Greenpeace
* 12 EUR pa (2GB) + 0.25 EUR per month per GB extra

[Mailfence](https://mailfence.com/en/)

* Own domain: Yes, 10 aliases
* Location: Belgium
* Calendars: Yes
* Privacy: Document store	Very clear and thorough No ads, no selling, no tracking Honest
* Security: End-to-end OpenPGP (Integrate keystore: own control or mastered on their servers), 2FA, DKIM option
* IMAP/SMTP: Yes
* Notes: Good functional browser based app, includes document store.  Integrated keystore interesting concept, might want to consider security implications of sending private keys to browser but they are clear on how they do this and it appears a good solution.  Good blog and very clear content Lots of really useful walk-throughs and knowledge base posts. supports EFF.
* 30 EUR for 5GB pa

[ProtonMail](https://protonmail.com/)

* Domain: yes with aliases
* Location: Switzerland
* Calendars: Yes
* Privacy: Anon signup, No ip logs, – *they don’t say the won’t sell data but it may be implied*
*	Security: end-to-end openPGP
* IMAP/SMTP: Yes,sort of Beta version of a bridge app that runs on device.
* Notes: Bespoke open source apps - Nice browser based app UI. Big in media Sponsored by CERN (gov related?). They also have VPN.  They don't make the cost of service obvious on site and offering feeling very monetised.  Privacy statements are not straight forward.
*	75 Eur (5 GB) pa

At first I went with Tutanota as I imagined they would provide everything I needed (I wrote the summary above after using it).  I soon realised that it didn't really offer me what I needed.  If they supported bespoke domains (and outbound PGP) then I'd go with Posteo as I like the features of the service plus their work ethics.  I don't find proton mail to be as transparent with it's privacy policy or its pricing models (I find them a little commercially driven).  Mailfence appears corporate on their landing page but once reading the extensive blog posts and knowledge base it starts to feel much more techie-boutique - they have all the features I need, the price seems reasonable and they support EFF and EDRI.

I've been using Mailfence for a week or so now and I'm pleased with the service and email integration.  Although they support CalDav and I can view calendars in my own organiser apps they don't allow creating and managing events through third party apps... I'll have to see if I can live with this or what alternatives I can do to work around it.

This is only one part of the step to removing dependency on Gmail/Google - next up: doc management and whilst I have a doc store with Mailfence I want to look at more feature rich options as replacements for Google Drive.
