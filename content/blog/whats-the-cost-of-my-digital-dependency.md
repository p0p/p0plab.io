---
title: "What's the Cost of My Digital Dependency?"
date: 2019-01-22T11:57:06Z
description: "An experiment to reduce my dependency on digital consumer providers who have access to my personal data and use it in uncertain ways."
---
I am very much a consumer of digital products from only a handful of service providers.  These tech giants offer me all the digital tools I need and I have become dependent on them for most aspects of my life.  Many of these products are free to use and they're extremely accessible and convenient.  In some cases I have paid for licences though they are few and far between. Worst still, in some cases (e.g. my phone) I am forced into using them and accepting their terms. All of them can access my personal data and all of them use that data for their commercial (and possibly political) gain.

My data is used in ways I don't really understand.  So what's the real cost?

There are too many stories in the news of how data has been abused for commercial or political gain.  Tech giants and associates are often in court and receive hefty fines for the misuse of our data.  Directors of large social media platforms openly state they wouldn't let their family use their own services.  Each time a new story hits the headlines we are surprised that what happened was possible or amazed it was allowed, we are stupefied that we accepted it or shocked that we we were led into a false sense of security.  We are becoming used to having our perceptions challenged, hardening us to what we will tolerate.

We expect these companies to change their ways, mainly they pay their fines, publicly apologise and continue with slight amendments.  Do we ever consider changing ourselves?

I see emerging ways in which our data is used.  Data such as from when and how we use our devices, of who we are physically present with is being used and our health statistics and biometrics are used for such things as insurance premiums and credit references.  The way by which we our views and opinions are manipulated becomes ever more sophisticated and the more this ebbs into our physical lives the more we will continue to hear the hyper-sensational headlines.

My data is a precious commodity, my data is me.  What can I do to protect myself?

I don't want to give up using the internet, the connected devices, the digital products and the services that I depend on day to day.  I couldn't.  I need them for work. I need them to keep in touch with friends and family.  I need them to manage my finances and pay the bills.  I need them to function within society. I'm on a mission to explore the alternatives, to reduce my data footprint and limit the access to my digital self.  It's not going to be easy, I'm really tied into Google and Amazon amongst others, what they offer is so appealing functionally and there will be compromises I have to make on all sides.  Hopefully what I explore and share here will help me, my family and others too.
