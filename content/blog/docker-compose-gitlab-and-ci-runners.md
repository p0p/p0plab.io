+++
title = "Using Docker Compose to run Gitlab and CI/CD Runners"
author = ["Tom Broughton"]
tags = ["docker", "gitlab"]
categories = ["technology"]
draft = true
series_weight = 4
series = "Self Hosted Gitlab in Docker"
+++

I am exploring self-hosted Gitlab and I want to set up [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
that will run every time I commit new source/content to a project and
will publish to (self-hosted) Gitlab Pages.

I'm already [Running Gitlab CE in a Docker Container]({{< relref "gitlab-in-docker-container" >}}) but now I'm going
to stop and remove that container (see bottom of that post) and
configure Docker via `docker-compose`.

and I now want to
set up a CI/CD pipeline for a project so that every time I commit a
change in my code it will build me some Gitlab Pages.

I've created a project nam a basic "hello world" html

I already have Gitlab running in a
