---
title: "Flashing TockOS onto nRF52840 Dongle"
date: 2020-02-24T10:25:13Z
description: ""
draft: true
---

## Installing Dependencies ##

1. Rust `sudo pamac install rust`
2. Rustup `sudo pamac install rustup`
3. OpenOCD (I already have this installed)
4. pyocd (in a python virtual env I have it installed with pip)
5. The TockOS project `git clone https://github.com/tock/tock.git`

