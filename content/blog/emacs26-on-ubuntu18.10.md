---
title: "Emacs26 on Ubuntu 18.10"
date: 2019-04-09T23:52:00+01:00
description: ""
---

I've installed emacs 26  on Ubuntu 18.10 with the following config and dependencies:

We need the gnutls library so that packages can be installed in emacs of https.
To do this I seached for the latest libgnutls-dev package in my ubuntu distro apt repos

```
$ apt-cache search 'libgnutls.*-dev'
libgnutls28-dev - GNU TLS library - development files
$ sudo apt-get install libgnutls28-dev 

```

I also installed other dependencies

```
$ sudo apt install build-essential libncurses-dev
```

Get the download of the latest emacs from https://www.gnu.org/software/emacs/download.html (link to one of the mirrors) 

```
$ wget http://mirror.koddos.net/gnu/emacs/emacs-26.1.tar.gz
$ wget http://mirror.koddos.net/gnu/emacs/emacs-26.1.tar.gz.sig
```

Get the gnu keyring of public keys

```
$ wget https://ftp.gnu.org/gnu/gnu-keyring.gpg
```

Add the gpg gnu keyring and check the signature against the file

```
$ gpg --import gnu-keyring.gpg
$ gpg --verify emacs-26.1.tar.xz.sig emacs-26.1.tar.xz

```

Extract the files
```
$ tar -xzvf emacs-26.1.tar.gz
```

Addition 2018-04-18: I wanted to use irc client weechat that uses dbus for notifications, I had to install a library into ubuntu and reconfigure and reinstall so complete this next step to do that and then continue:

```
$ sudo apt-get install libdbus-1-dev
```

```
$ ./autogen.sh
```


configure (I'm using the flag --without-x as I won't be using a gui)
```
$ ./configure --without-x
```

run make
```
$ make
```

and then install the binaries
```
$ sudo make install
```

You should then log out and back in again so that /usr/local/bin is in your $PATH and then you can run

```
$ emacs
```


I've then installed markdown-mode to edit hugo markdown files: ```M-x install-package RET markdown-mode```
