+++
title = "Highlighting Source Code with Ox-Hugo"
author = ["Tom Broughton"]
date = 2020-03-09T17:53:00+00:00
tags = ["web development"]
categories = ["technology"]
draft = true
series_weight = 4
series = "Making of this Site"
+++

Code highlighting in Hugo is managed via
[Chroma a general purpose
syntax highligher](https://github.com/alecthomas/chroma) written in go.  To enable the markup style I've
added a section within my config.toml to configure chroma.

```toml
[markup]
  [markup.highlight]
    codeFences = true
    guessSyntax = false
    hl_Lines = ""
    lineNoStart = 1
    lineNos = false
    lineNumbersInTable = true
    noClasses = true
    style = "monokai"
    tabWidth = 4
```

When I then export from org mode using ox-hugo the code I have written
between `#+begin_src` and `#+end_src` is rendered into code blocks and
the highlighting is applies.  Chroma knows what to highlight based on
the [type of programming language I specify](https://github.com/alecthomas/chroma#supported-languages) `#+begin_src`.  For
example, `#+begin_src org` or `#+begin_src emacs-lisp` or `#+begin_src
python` etc.  I'm finding it useful looking in the source of the
`lexer` that I want to use to get the correct reference for the
language, eg - I found `go-html-template` in the `lexers/g/go.go`
file.

<!--more-->

There are [a number of styles available for chroma](https://xyproto.github.io/splash/docs/longer/index.html) that can be
configured within the config or `[markup.highlight]`.  This means I
don't have to create or import style sheets for highlighting code,
it's generated for me.

I can also highlight lines of code by adding `hl_lines` to the hugo
front matter.  ox-hugo has a nifty way of appending this to the export
by adding `:hl_lines n[,...]` to the `#+begin_src` line.  For example
to highlight the lines in a python block to show where the time module
has been used I could do the following:

```org
#+begin_src python :hl_lines 2, 5
# a simple and annoying loop in python
from time import sleep

while true:
    print("hello world")
    sleep(1)

#+end_src
```

Which will result in the following source block being rendered in html
via hugo.

```python { hl_lines=["2"," 5"] }
# a simple and annoying loop in python
from time import sleep

while true:
    print("hello world")
    sleep(1)
```
