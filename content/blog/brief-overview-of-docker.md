+++
title = "A Brief Overview of Docker"
author = ["Tom Broughton"]
date = 2020-04-12T16:52:00+01:00
tags = ["docker"]
categories = ["technology"]
draft = true
series_weight = 2
series = "Self Hosted Gitlab in Docker"
+++

Docker and Docker Compose run on almost every platform (x86, Arm, etc) and every
distribution (linux flavour x, BSD, Mac OSX, Windows...) so it's
become the ubiquitous containerisation technology for most enterprise
and increasingly personal use.  Here I'll give a pointer on how I
installed for my <span name="install-prod">local</span>
machine and some of the basic principles and commands.

<aside name="isntall-prod"> I once installed Docker Compose from a
package on Ubuntu using `apt` and when I logged an issue in Github it
was because I hadn't [installed Docker Compose using the instructions
on the Docker site](https://docs.docker.com/compose/install/), the apt package was an older version and missing
the fixes I needed.  If I were installing in production I'd follow
those instructions.  </aside>

I'm going to use docker for various local development tasks and not
for production on this machine.  For expediency I have used a package
manager for my distro which is Arch Linux so I'm using `pacman` but
there will be packages available for most if not all package managers
such as `yum` or `apt`.

Docker builds containers based on Docker Images which are built from
Dockerfile (a script that runs shell like commands).  In my case I
won't be bothered with building any images, I'll use [images that are
built and maintained by Gitlab](https://docs.gitlab.com/omnibus/docker/).  I'll cover how I am using these
images in following posts.

Docker will pull images to a local repository and create containers
from them. `docker image ls` will list all the local images in the
repo.  Of course there will be nothing listed when I initally install
Docker but I've recently been working on getting Gitlab running in
Docker as well as some MySQL development for work.

```bash
$ docker image ls
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
gitlab/gitlab-runner-helper   x86_64-4c96e5ad     04825cc71bbf        21 hours ago        57.5MB
gitlab/gitlab-ce              latest              e6b464b98aff        12 days ago         1.92GB
mysql                         5.7                 413be204e9c3        12 days ago         456MB
alpine                        latest              a187dde48cd2        2 weeks ago         5.6MB
gitlab/gitlab-runner          latest              6c50485db1fc        3 weeks ago         390MB
```

Typically a container is initialise using `docker run <image name>`
which will pull the image if it's not on the local machine and create
and start a container from it.
<span name="hello-world">Containers</span> can be started
or stopped and there's a command `docker ps` to see all running
processes (containers) and docker container ls -all to see all
containers on the machine.  Containers, once stopped, can be deleted
with `docker rm <container name or ID>`.

<aside name="hello-world">
I'm a massive believer in learning by doing, the great thing about
docker is that I know I can do very little wrong.  For instance
creating containers won't splay stuff all around my file system and
containers are meant to be created and destroyed. So I ran this and
tried out some of the docker commands:

```bash
$ docker run hello-world
```

</aside>

The only issue with creating and destroying containers is persisting
data they create and need.  For example it would be pointless creating
a Gitlab server container and then delete all the git data with it
when destroying the container.  Volumes can be configured so that the
container's directories are actually linked to the host machines'
directories.  With Gitlab for example the container uses
`/var/opt/gitlab` to store it's data but a volume can be set up to the
host machines filesytem such as `/srv/gitlab/data`

Docker also creates networks that bridge containers so they can access
each other and can access the outside world.  Accessing the docker
container from the host (or outside world) is largely down to the
setup outside of docker but docker maps ports.  For example in the
Gitlab server container ports are mapped `80:80`, `443:443` and
`22:22` which are straight through mappings, <http:localhost> will route
straight to port 80 on the docker container.  These can also be
changed so setting a container with port mapping `8080:80` will map
port 8080 on the host machine to port 80 in on the container.

[Docker Compose](https://docs.docker.com/compose/) is a separate application that manages docker
containers and networks in a single yaml file.  It also acts as a
docker wrapper and a lot of the container commands that can be run in
docker can also be run in `docker-compose`.

The main Docker Compose commands are `docker-compose up` which will
create the relevant containers if needed and start them running with
the various settings in the docker-compose.yml= file and
`docker-compose down` which will stop and remove the containers.
