+++
title = "Using Ox-Hugo to Structure Content"
author = ["Tom Broughton"]
date = 2020-03-04T19:34:00+00:00
tags = ["web development"]
categories = ["technology"]
draft = true
series_weight = 2
series = "Making of this Site"
+++

I'm writing the content for my site in org-mode within Emacs and I'm
using ox-hugo to export the content into markdown for hugo then to
publish as static html.  It sounds a complex tool chain but it's set
up to work in way I find very organised and very productive.

<!--more-->

All text and metadata is in one org file.  The content that gets
published into posts is managed one long org file.  I could have one
org file per post but I think this defeats the purpose of using
org-mode.


## Hugo Leaf Pages and Branches. {#hugo-leaf-pages-and-branches-dot}

In Hugo a leaf page is more or less a content page and a branch
page are listings of pages, typically site sections.

Org headings with the property `:EXPORT_HUGO_SECTION:` set result
in site sections.  Most of the top headings I have in my org file
are sections.  I've grouped the content into different sections,
one for each of my main interests.  I wondered whether I should
just have one big bucket for all content but for some reason I
feel this is too messy for me to manage.  I know that some
content will overlap, for instance I have a section for
technology and a section for creativity but I do a lot of
creative things with technology.  However, I've decided to take
this route for now and will add new content to the section I feel
it belongs to most.  I have other ways of grouping content on the
site too which I'll describe shortly.

All other headings that have a `:EXPORT_FILE_NAME:` value set
within a `:PROPERTIES:` drawer will export as either a post (hugo
leaf) or a listing page (hugo branch).  I group headings together
under their relevant section heading and along with the
`:PROPERTIES:` drawer I simply write content.

The site main menu has an entry for each section I've created.
To add a section to the "main" menu I use another property in the
`:PROPERTIES:` drawer, `:EXPORT_HUGO_MENU: :menu "main"`.  All
sub-headings will inherit properties unless they are over-ridden.

Because I don't want all posts within a section to be added to
the main menu for every section I create I also create a
sub-heading that will act as the listings page for that section.
This means that I can add this listings page to the main menu
without all other posts in that section being added.  To do this
I simply define the filename property as `_index` so that it
creates a listing page (hugo branch).


## Grouping Content {#grouping-content}

To group content in other ways on the published site I use tags.
Tags are first class citizens in org-mode as are categories
(which I have yet to decide if I'll use).  `C-c C-c` on a heading
prompts for me to enter tags for that entry in org-mode.  These
tags are automatically published using ox-hugo.

I've also created my own taxonomy called "series" which I'll use
to group content together in a specific order, so that I can have
a number of posts that follow on from each other with a specific
purpose.  This post is in fact a part of a series.  I have
entered a `[taxonomies]` section within the hugo's `config.toml`
to define the new taxonomy `series`.

I've used an ox-hugo property to set the custom front matter so
that the "series" will get published. Within the `:PROPERTIES:`
drawer of the post I've entered
`:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :series my_series` so that
when ox-hugo publishes this post it will publish =series =
"my_series" into the content's front matter.

Series are ordered using taxonomy weighting.  Because I've set up
"series" as a taxonomy I can use taxonomy weighting to order the
post within the series.  Therefore I can set the weight of the
first post in a series to 1, the second to 2, etc.  I may also be
able to use this value later on in my theme and templates to
display the number within the site against the post title when
necessary.  To set the weight I am using another property
`:EXPORT_HUGO_WEIGHT: :series 2`.  Then as long as each post is
only relevant to at maximum one series then this weight should
work.

To group series content together within the org file I list them
under a heading marked with **SERIES**.  Because this heading has
no export properties it doesn't result in any content or sections
being created.  This way I can write all the content for a series
in one area of the org file and collapse or expand the series
heading as required.


## Drafts and Published Posts {#drafts-and-published-posts}

`TODO` and `DONE` are org-mode status' against each post heading that
define whether a post is in draft or is published.  Within org-mode
there's the concept of `TODO` items, these are headings that have been
marked with `TODO`.  Once the post is ready to publish this value can
be changed to `DONE`.  I have other status' `WIP` and `REVIEW` set up
too, each of these are currently treated as `TODO` but I just use them
to help me keep a track of how far I have got in writing each post.

A published date is recorded when publishing a post.  When I mark a
post as published by setting the status to `DONE` a `CLOSED` date is
added to the post's metadata.  I use the dates within the published
posts and also as part of the url permalink.

When a post is in draft I use an `:EXPORT_DATE:` so that ox-hugo
publishes a date into the draft content front matter.  This date is
useful when I am previewing the site in `-D` draft mode locally.
I set this date when I first create a post by adding the property to
the `:PROPERTY:` drawer and adding a time stamp, `C-u C-c !`.


## Publishing {#publishing}

When I'm happy with a post then I export the content from org into
hugo markdown using ox-hugo.  The export command in Emacs is `C-c
C-e`, this brings up a prompt and then `H` for hugo format and `A` for
the whole site or `H` to just export post (sub-heading) I'm focused on
in the org file.

This then exports any necessary markdown to the content directory in
my hugo project.  If I'm viewing the site locally then the site
preview automatically updates.  If I'm publishing online then I add
and commit the new hugo content into git and push to my remote
origin.  I'm using gitlab pages and gitlab CI which runs a hugo
publish job whenever it receives new commits.
