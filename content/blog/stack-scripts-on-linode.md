---
title: "Stack Scripts on Linode"
date: 2019-03-26T10:26:58Z
description: ""
draft: true
---

I'm using linode instead for AWS EC2s as an alternative to using Amazon in my effort to reduce my dependency on the big tech giants.

One thing you can do to automate the build of a server is create a stack script that can run commands.  It's basically a bash script - useful to reduce some of the work I usually do manually.

The script exposes some user defined fields that are part of the set up process in the Linode UI - these would typically be handled using read parameters from the console.

```
#!/bin/bash
#<UDF name="USERNAME" label="Username">
#<UDF name="USERPASSWORD" label="Password">
#<UDF name="USERPUBKEY" label="User SSH public key" default="">
#<UDF name="HOSTNAME" label="Hostname (FQDN)" default="">

# Last update: March 26 2019
# Author: Tom Broughton

set -e

if [ ! -z "$HOSTNAME" ]; then
  hostnamectl set-hostname $HOSTNAME
  echo "127.0.0.1   $HOSTNAME" >> /etc/hosts
fi

# Set up user account
adduser $USERNAME --disabled-password --gecos ""
echo "$USERNAME:$USERPASSWORD" | chpasswd
adduser $USERNAME sudo

# If user provided an SSH public key, whitelist it, disable SSH password authentication, and allow passwordless sudo
if [ ! -z "$USERPUBKEY" ]; then
  mkdir -p /home/$USERNAME/.ssh
  echo "$USERPUBKEY" >> /home/$USERNAME/.ssh/authorized_keys
  chown -R "$USERNAME":"$USERNAME" /home/$USERNAME/.ssh
  chmod 600 /home/$USERNAME/.ssh/authorized_keys
  sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
  echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
fi

# Disable root SSH access

sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config

```