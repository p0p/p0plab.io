# My Site #

I write about things and want to gather them together on my site.
Whoe is this site for and how will they use it or why would they be
here? 

1. Me
  - to have a creative outlet
  - to remember who I was, what I did, how I did it etc
  - to work on projects and keep a log of where I am with them
  - so that a part of me might live on longer than my biological self
2. Daughter
  - to be encouraged to be creative
  - to remember me
3. Randoms
  - to see how I've managed to solve a problem they are also faced
    with
  - People who are dealing with similar mental health problems I've
    dealt with
  - learners of counselling skills who might be on a similar learning
    journey I am on
  - people who might be interested in following a particular series
    because it interests them
4. Publishers
  - There may be books that emerge from this and they might want to
    see them in action
5. Employers
  - To see who I am and what I have done
  
## Sections  ##

  - Technology
  - Creativity
  - Mental Health
  - Adventure
  - Food

## Series ##

  - Co-Creatives
  - Big Wave
  - Thinkpad
  - Podx
  - User Guides for Me
  - Adventure Labs
  - Counselling Skills
  - The Porregg Book
  - Bread
  - Where On Hearth Are We?
  - Water of life
  
## Tags ##

 - Technology
 - Crafting
 - Hacking
 - Designing
 - Making
 - Digital
 - Web
 - Coding
 - Mental Health
 - Self Care
 - Counselling
 - Microcontrollers
 - Thinkpad
 - Linux
 - Strategy 
