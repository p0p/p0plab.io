---
title: "Flashing Micropython onto nRF52840 Dongle"
date: 2020-02-21T23:32:51Z
description: "I describe how I have flashed micropython onto the nRF52840"
---
This is perhaps the first in a series of personal investigations into
the
[nRF52840-Dongle](https://www.nordicsemi.com/Software-and-Tools/Development-Kits/nRF52840-Dongle
"go to Nordic site ").  The dongle first attracted my attention when I
was looking into usb dongle devices with microcontollers for a new
Adventure Labs kit.  Shortly afterwards it was referenced on a Linux
news podcast I listen to mentioning the [Google
OpenSK](https://github.com/google/OpenSK "Github repo for Google
OpenSK") which can be flashed onto this device as a secure key.  This
has piqued my interest as I've considered getting a yubikey for some
time but being proprietary it's put me off.

## So Why Micropython? ##

OpenSK installs onto [TockOS](https://www.tockos.org/ "go to tock os
website") and is written in rust.  However, the guides on the OpenSK
site states the need for a JTAG programmer and these can be quite
expensive.  An alternative programmer can be via SWD, and there are
various ways of programming the nRF52840 via SWD. However, the
comments on OpenSK's github issues suggest that this hasn't been
successfully implemented yet.

There is a way of installing OpenSK onto a chip that already had
TockOS and there are guides on installing TockOS via OpenOCD for other
chips.  Again, however, the guide to install TockOS onto the nRF52840
specifies using the JTAG programmer and a tiny connector.

[Micropython has a port for the nRF52840
dongle](https://github.com/micropython/micropython/tree/master/ports/nrf)
and has guides for flashing the firmware onto the dongle using
various methods via SWD.

Therefore maybe it's possible to burn TockOS onto the nRF52840 if I
can follow the same steps as developers of Micropython but use the
binaries for TockOS.

Also I'd like to get familiar with the chip and the dev board,
using Micropython will give me a development environment which I'm
familiar with to explore some of the features.

## Preparing the Dongle ##

Underneath there are 10 small pads, 5x2 rows, [designed for a
specific connector
TC2050](https://www.digikey.com/product-detail/en/tag-connect-llc/TC2050-IDC-NL/TC2050-IDC-NL-ND/2605367).
This is a connector for a JTAG programmer and most of the guides
I've come across recommend this method for burning new bootloaders.
looking at [the Segger site for a J-Link programmer and the necessay
cables etc](https://www.segger.com/purchase/pricing/jlink-related/) I
estimate the required equipment to cost £100s...
  
But there is an alternative.  Next to these pads are 10 rectangular
pads that can be used with other Serial Wire Debuggers - two of these
pads, the CLK and the DATA are broken out on the the edge of the
board, near to the usb connector.  It is here I have soldered pins to
connect jumper wires.  I've also soldered a pin onto one of the GNDs too.

![top down rendering of the dongle](nRF52480Dongle.png "SWDIO and SWDCLK near USB connector, GND near the aerial end")

## Wiring up the ST-Link Programmer ##

I'm going to use an ST-Link SWD programmer [and I've written a post
about setting this up.](/blog/using-stm32nucleo-as-st-link-programmer).  In
that post I make sure that I have openocd installed through my os
package manager and pyocd installed through pip into the python
virtual environment I'm working in.  From this point on note that I
have these installed and I'm working in the virtual env.

I connect the nRF52840 to the ST-Link in the following way, without
VDD or RESET.

  | nRF5280 | ST-Link |
  | ------- | ------- |
  | SWCLK   | CN2 p2  |
  | GND     | CN2 p3  |
  | SWIO    | CN2 p4  |

![diagram showing the jumpers off for st-link mode on the nucleo and the SWD pins on CN2](st-link-on-nucleo.png)

## Burning Micropython onto the nRF52840  ##

I cloned Micropython from github and built the project

```
git clone https://github.com/micropython/micropython.git Micropython
cd micropython
make -C mpy-cross
```

I then change directory to `./ports/nrf` and plugged both the ST-Link
and the dongle into my PC.  I then ran:

```
make submodules
make V=1 BOARD=10059 FLASHER=pyocd flash 
```

The output should list lines like `INFO:pyocd.coresight....` and then
show an upload progress bar.  Once complete micropython should be on
the dongle.  If there are any issues maybe check the wiring, check
there ST-Link can be seen (run `pyocd list`) and check the error
messages.

I didn't have any issues to resolve.

![dongle and st-link plugged into laptop and SWD with jumper wires](flashing-nrf.jpg "Both dongle and ST-Link plugged into USB ports, jumper wires connect CLK, DATA, GND")

## Testing Micropython on the nRF52840 Dongle ##

I am using the repl on the chip and accessing it using `picocom`.

```
picocom -b 115200 /dev/ttyACM0 
picocom v3.1

port is        : /dev/ttyACM0
flowcontrol    : none
baudrate is    : 115200
parity is      : none
databits are   : 8
stopbits are   : 1
escape is      : C-a
local echo is  : no
noinit is      : no
noreset is     : no
hangup is      : no
nolock is      : no
send_cmd is    : sz -vv
receive_cmd is : rz -vv -E
imap is        : 
omap is        : 
emap is        : crcrlf,delbs,
logfile is     : none
initstring     : none
exit_after is  : not set
exit is        : no

Type [C-a] [C-h] to see available commands
Terminal ready
MicroPython v1.12-167-gf020eac6a on 2020-02-24; PCA10059 with NRF52840
Type "help()" for more information.
>>> from machine import Pin
>>> grnled = Pin(6, Pin.OUT)
>>> grnled.off()
>>> # Green LED has turned on!
```

## Conclusion ##

Once I had the ST-Link set up I found flashing micropython onto the
nRF52840 very straight forward.  I notice in the Makefile there are
commands using the pyocd programme which take a .hex file as a
parameter.  The next step for me is to try this same process with
TockOS.



