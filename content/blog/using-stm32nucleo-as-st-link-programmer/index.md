---
title: "Using the STM32 ST-Link Programmer (updating the firmware)"
date: 2020-02-22T12:03:51Z
description: ""
---
I have used the
[stm32Nucleo-F411RE](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-nucleo-boards/nucleo-f411re.html)
as a ST-Link programmer which uses [Serial Wire Debug
(SWD)](https://www.silabs.com/community/mcu/32-bit/knowledge-base.entry.html/2014/10/21/serial_wire_debugs-qKCT)
as an alternative to JTag programming to burn the a bootloader onto
the nRF52840. To configure the STM32Nuclueo as ST-Link I removed the jumpers on
CN2.

I installed [openocd an alternative to segger j-link programming
software](http://openocd.org/) `sudo pamac install openocd` and then
within a python virtual environment I `pip install pyocd` - [pyocd - a
python interface making use of the openocd
api](https://pypi.org/project/pyocd/).

Running `pyocd list` should auto detect the ST-Link if it's plugged
into the USB port.  I could have set up udev rules but I ran this as
sudo as I'm using a test laptop I'm not too concerned about.

When I ran it I received an error stating I needed upgrade the
firmware on the ST-Link... 

## Updating the ST-Link Firmware ##

I needed to update the firmware on the ST-Link module of
the Nucleo using [STMs st-link firmware
updater](https://www.st.com/en/development-tools/stsw-link007.html).
Because I'm running Linux (manjaro) on my pc I didn't have a java
runtime installed I had to `sudo pamac install jre-opensdk` and then
downloaded firmware with `sudo java -jar STLinkUpgrade.jar`.

Note that the readme in the firmware download instructs Linux users to
run make to install the upgrader but looking at this file I can see it
does quite a few things including setting up udev rules and also has
some kind of binary blobs involved.  I don't see the need for these
and didn't want to risk poisoning my system so I just ran the jar
directly.

The Upgrader GUI that loads is possibly the ugliest GUI I've ever come
across but the functionality is simple enough.  It detected the
ST-Link plugged into the USB port and I was able to simply follow the
buttons and onscreen messages without any issues.

## User Access to the USB Device ##

Lastly I didn't want to continue accessing the device using sudo so I
created my own rule.  The Serial port (on Manjaro/Arch) is owned by
root and is in the group uucp, and my user is also added to the uucp
group.  This is usually sufficient for reading and writing to the
serial ports.  However, for some reason, running `pyocd` I'm getting
the error `no available debug probes connected.`

To fix this I've created a udev rule
`/etc/udev/rules.d/50-st-link.rules

```
SUBSYSTEM=="usb", ATTR{idVendor}=="0483", ATTR{idProduct}=="374b", MODE="0666"
```

(I sourced the vendor and product id from `sudo dmesg` when I
connected the ST-Link to my pc).

## Conclusion ##

Now running `pyocd list` returns:

```
  #   Probe            Unique ID
-------------------------------------------------
  0   STM32 STLink     066EFF495056805087175284
```

Maybe I could have used the .sh scripts from STM32 but with a load of
binary data in there that I couldn't decipher I didn't feel
comfortable, instead I'm happy with this manual process, it's a one
off really - now my STM32 ST-Link firmware is updated I should be able
to use it as a programmer using OpenOCD and PyOCD. 

[Flashing Micropython onto the nRF52840 Dongle as example of how I have used the ST-Link.](/blog/micropython-on-nrf52480/)
