---
title: "Watching CPU Temp and Fan Management Thinkpad"
date: 2019-03-10T20:20:16Z
description: ""
---

I have a Thinkpad T430 and I've upgraded the stock i5 dual core processor with an i7 quad core all bought off eBay at a ridiculously low price considering these high wattage 3rd gen CPUs are provide better performance and processing capability than the modern 8th gen low power quad core cpus.  The i5 was actually a 35W CPU whereas the quad i7 is 45W, of course the power is disipated into heat resulting in the fan needing to be on more to cool down the CPU.

However, the stock fan control also compensates for the running of a Sata HDD and the heat it creates spinning away.  I'm not using a HDD, I've an SSD and so the heat isn't there, that means I can run the fan at slightly higher CPU levels as it's not having to compensate for the extra HDD heat.

I'm going to use a couple of programmes:

* the first is lm-sensors and I will use a KDE widget called [system monitor dashboard](https://store.kde.org/p/1274640).  This will allow me to detect the sensors in the laptop (namely temp sensors on the cpu) and monitor the readings.
* As I have a thinkpad the second is thinkfan, a fan control utility that can be configured to set the speed of the fan at different temp readings.  This way I can make sure the fan only turns on when needed and at slightly higher readings than the stock config for the fan which results in the fan on most of the time.  Alternatively you could look at using other [fancontrol](https://packages.ubuntu.com/search?keywords=fancontrol) utilities.

## lm-sensors

```
$ sudo apt-get install lm-sensors
```

Then scan for sensors ```sudo sensors-detect``` - say 'YES' to everything and add the recommendations to the ```/etc/modules``` file.

And then load the new modules ```sudo service kmod start```

I then installed the [system monitor dashboard](https://store.kde.org/p/1274640) as I'm using KDE (plasma) desktop, you can simply just type ```watch sensors``` in a terminal to view the CPU temps.

## Fan Control (thinkfan)

```
$ sudo apt-get install thinkfan
```

Create the config ```sudo nano /etc/modprobe.d/thinkfan.conf``` and add the line to activate control of fan to the thinkpad_acpi (advances configuration and power interface):

```
options thinkpad_acpi fan_control=1
```

Edit the thinkfan default source file (that the deamon uses when initiating) and enable thinkfan to start ```sudo nano /etc/default/thinkfan``` and add the line:
```
START=yes
```

## Configure fan speeds

You can see the temp of the cpu sensors, for me currently this is:
```
$ sensors
coretemp-isa-0000
Adapter: ISA adapter
Package id 0:  +44.0°C  (high = +87.0°C, crit = +105.0°C)
Core 0:        +36.0°C  (high = +87.0°C, crit = +105.0°C)
Core 1:        +44.0°C  (high = +87.0°C, crit = +105.0°C)
Core 2:        +40.0°C  (high = +87.0°C, crit = +105.0°C)
Core 3:        +44.0°C  (high = +87.0°C, crit = +105.0°C)

acpitz-virtual-0
Adapter: Virtual device
temp1:        +43.0°C  (crit = +200.0°C)

thinkpad-isa-0000
Adapter: ISA adapter
fan1:        2564 RPM
```

We are going to monitor the core temps and then set different fan speeds at different thresholds.  The first thing we need are the paths to the sensors:

```
$ find /sys/devices -type f -name "temp*_input"
/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp3_input
/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp4_input
/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input
/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp5_input
/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp2_input
/sys/devices/virtual/hwmon/hwmon0/temp1_input
```

Next we want to add the paths to the think pad config
```
sudo nano /etc/thinkfan.conf
```
Uncomment the line ```tp_fan /proc/acpi/ibm/fan``` and add a line for each 'coretemp' sensor path (in my case using 'hwmon' rather than 'tp_thermal' or 'sensor').

And then create the curve for the sensor temp and fan speed...  1-7 are the different speeds, 127 if full speed and the temps are min and max (increase to and decrease from) thresholds.  This is what I have in my file (I basically increased the default by 5 degrees).

```
hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp3_input
hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp4_input
hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input
hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp5_input
hwmon /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp2_input

(0,     0,      60)
(1,     53,     65)
(2,     55,     66)
(3,     57,     68)
(4,     61,     70)
(5,     66,     75)  
(6,     71,     80)
(7,     76,     90)
(127,   85,     32767)
```

BE CAREFUL WHEN CHANGING THESE SPEEDS, THIS CAN DO SERIOUS DAMAGE TO YOUR CPU IF YOU DON'T SET ADEQUATE THRESHOLDS.

Save and test to see if the thinkfan utility works with this config (you'll get  a warning to double check your temp settings etc).  ```$ sudo thinkfan -n```.  If all is working well add the service to startup on reboot ```$ sudo update-rc.d thinkfan enable```.

And then reboot.
