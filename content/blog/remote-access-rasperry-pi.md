---
title: "Remote Access Rasperry Pi"
date: 2019-02-12T23:58:10Z
description: ""
---
## Outcomes

We are going to set up remote access to the raspberry pi with an SSH tunnel and optional VNC so that we can use the pi without it being connected to a monitor, keyboard and mouse.

We will use wifi to connect remotely to the pi. We'll use ssh which is a [secure shell](https://en.wikipedia.org/wiki/Secure_Shell) allowing direct command line access on the pi.  We can do most things from this command line but to make it more visual we will install and configure VNC which will give us remote access to a desktop environment.

## What you need

* Raspberry Pi with wifi (I'm using Pi Zero W)
* SD card Raspbian (with desktop for the VNC option)
* A computer to use to remotely access the pi
* A wifi you are connected to and to which you can connect the pi

## Getting the Pi onto the Wifi

First off we need to configure the wifi and allow ssh before we insert the SD card into the pi and turn it on.  Using a USB card reader or SD slot on your computer we need to create two files on the SD card.

First file is for the wifi and it's file name is `/boot/wpa_supplicant.conf`

{{< highlight sh >}}
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=«your_ISO-3166-1_two-letter_country_code e.g. GB»

network={
    ssid="«your_SSID»"
    psk="«your_PSK»"
    key_mgmt=WPA-PSK
}
{{< /highlight >}}

Second is just an empty file `/boot/ssh` which instructs raspian to allow ssh access.  

We can now insert the SD card into the pi and turn it on.

## SSH - Secure Shell tunnel

Once the Pi has booted up we can use our computer to ssh to it. On linux or mac this is straightforward, open up a console/terminal window and type `ssh pi@raspberrypi` and you should be prompted for a password which is `raspberry` you may be asked to trust the connection/device you're connecting to and add it to your computer's list of known devices.

If you're using windows then you'll have to set up something like [PuTTy to ssh into the pi](https://www.raspberrypi.org/documentation/remote-access/ssh/windows.md).

If you get an error when trying to connect such as `Could not resolve hostname raspberrypi` then you'll have to access the router to see what IP address your pi has been given and use that instead (eg `pi@192.168.1.10`).  If you can't access your router then you might need to [use nmap to scan the wifi for ip addresses](https://www.raspberrypi.org/documentation/remote-access/ip-address.md).

## Setting up VNC

You can do everything you need on the command line through ssh, however, for a more graphical interface you can use the desktop although it can be quite slow at times as you're using it over the network.

In you SSH console run `sudo rasp-config` which will bring up some options to change:

1. _interfacing options_ and select _vnc > yes_ and then finish the raspi-config session.  
1. _Boot Options_  select _desktop / cli_ and select _desktop autologin_
1. _change user password_ - it's important to change this from the default raspberry but you'll need to remember it

Now we'll make sure we have the latest vnc packages on the pi:

{{< highlight sh >}}
$ sudo apt-get update
$ sudo apt-get install tightvncserver
{{< /highlight >}}

Then start the vnc server `vncserver :1 -geometry 1600x900 -depth 16 -pixelformat rgb565` everything after the `:1` here is configurable resolution detail and can be changed to suit your needs.  When you start the server for the first time it will ask you to create a password.

On your laptop or pc using a vnc client (I'm using KRDC - KDE remote desktop client) connect to the pi using the ip address and port _5901_ eg:  `192.168.1.10:5901`, you should get prompted for the password you created when starting vncserver and you might have to log in as the pi user too.

You should now be looking at the Raspbian Pixel desktop.

To get the vncserver to start on boot create a file `~/.config/autostart/tightvnc.desktop` with the following contents:

{{< highlight sh >}}
[Desktop Entry]
Type=Application
Name=TightVNC
Exec=vncserver :1 -geometry 1600x900 -depth 16 -pixelformat rgb565
StartupNotify=false
{{< /highlight >}}

If you ever need to stop the vnc server then ssh into the pi and run `vncserver -kill :1`

## Wrap up

You have now configured remote access to your pi.  You can go further and enable remote access to the pi from the internet which required configuring your router to accept inbound requests and forward them onto the Pi hostname/IP.  This goes beyond this guide and you should be careful when opening up your local network to the internet.

You should now be able to ssh into your pi or use a vnc client to control the pi desktop.
