+++
title = "C.V."
date = 2021-04-16T23:00:00+01:00
draft = false
[menu]
  [menu.main]
    weight = 1003
    identifier = "c-dot-v-dot"
+++

I am a leader in innovation and delivery for Public Service and
Charity Sector digital products and services.  I have an extensive
creative and technical background with over 20 years industry
experience.  During my time at GDS (Government Digital Service)
senior civil servants described me as a "purple person", somebody who
combines organisational design with technology transformation, and at
the BBC I was considered to be a bridge between editorial/audience
needs and online solution delivery.  I balance portfolios with
suitable ratios of BAU and R&amp;D to ensure business continuity whilst
driving progress in an increasingly uncertain world.  I use and train
others in lean and agile principals along with building and leading
"user first" focused multidisciplinary teams inclusive of user
research, user experience, technical and data architecture.  I have a
holistic approach, bringing vision and direction to realisation in
collaboration with internal stakeholders, external partnerships and
extended networks.  I believe real change comes from people and that
the health of an organisation largely depends on the cultures and
norms within it.  I am Autistic and have developed an acute awareness
of neuro-divergent needs, seeking opportunities to use my privilege to
champion diversity, inclusion and social justice whenever I can.


## Competencies &amp; Achievements {#competencies-and-achievements}


### Digital Leadership {#digital-leadership}

-   Owned and advised on development strategies for digital products
    and services for UK Government, BBC and National Charities such
    as National Trust, Power to Change and The Dog's Trust;
-   Advised the Minister for Cabinet Office and HM Treasury on
    effective strategies for various public services across Government
    with above £5M spend on technology or digital delivery;
-   Led on defining the tools and approaches for digital
    transformation at scale and built support teams to work with
    Govt. departments to support their
-   Contributed significantly to Digital Service Standards and UK
    Government Transformation Strategy;
-   Led the discovery and alpha of standards and assurance
    GDS/Cabinet Office internal tooling ensuring design decisions
    were rapidly made through user research and weekly design
    sprints.
-   Designed and managed legacy transformation of DevOps teams within
    the Ministry of Justice’s digital studio, owning roadmaps for
    legacy system migration and new infrastructure deployment.
-   BBC Technical Architect and discipline lead for technical
    architecture governance, design principles and standards setting
    up working groups and technical design forums across BBC online;
-   Member of BBC's North Leadership group successfully transitioning
    BBC Homepage, Search and Navigation development from London to
    Salford;


### Digital Innovation {#digital-innovation}

-   Reviewed opportunities for innovation across UK Civil Service
    including AI and Robotic Process Automation (RPA) in public
    service operations;
-   Mentored staff, partners and students in approaches to
    innovation including using the Cynefin framework and Wardley
    mapping.
-   Architect of BBC Homepage on AWS - the first public facing BBC
    product to be designed and delivered into cloud technologies
    leveraging automatic scaling to cope with highly variable load
    profiles reaching peaks of up to 1,000 unique users per second.
-   Hands on expertise in prototyping and development using Node JS,
    PHP MySQL, GraphQL, React, message queues and many cloud PaaS
    and SaaS technologies.
-   Maker-Educator in physical computing and internet of things.
-   Advocate of Open Source, Open Data and open standards, assisting
    in their adoption into the Digital Service Standards for the UK
    government.
-   Regular attender of conferences and events for emerging
    technologies and innovation practices with wide reaching
    personal network.


### Digital Delivery {#digital-delivery}

-   Agile practitioner and coach, certified Scrum Master
    since 2008. Introduced agile practices across a number of
    organisations at enterprise level - both within delivery teams
    and senior management.
-   Led multidisciplinary teams in design and delivery of many
    complex, distributed digital products and services specialising
    in content production, media publishing and audience
    participation.
-   Built teams to support transformation across Government
    focusing on user research, service design, technical
    architecture, data driven decisions.
-   Recruited and managed teams in London, Salford and Cardiff with
    three direct reports and overall team of 30 people to deliver
    and support the Homepage, Search and Navigation for BBC Online.
-   Introduced and championed emerging roles and practices into
    multidisciplinary including "developer in test" and "devops".


## Career {#career}


### Aerian Studios - Solution Architect: July 2020 - Present {#aerian-studios-solution-architect-july-2020-present}

As Solution Architect I work closely with clients to understand
their requirements and I translate those into technical design and
requirements for our engineers to deliver.  I am very hands on and
will build out code, prototype new technologies and objectively
assess the best solutions inline with our cutting edge principles
and approaches. This is a part time position alongside Polydigital
and personal studies in Counselling Skills.


### Polydigital - Director:  Jan 2018 - Present {#polydigital-director-jan-2018-present}

I set up Polydigital to work across numerous organisations'
digital transformation strategies who's focus is on the public
benefit and social good.  I have been commissioned to review
digital product and service strategy and organisational design by
Power to Change, The National Trust and the BBC.  I have worked on
research and development projects including a Catalyst funded
investigation into issues with Universal Credit and development of
software for remote environmental spectrometers. I also develop
and run a scheme of work called Adventure Labs for schools and
youth groups teaching children about innovation and teamwork in
real world outdoor adventure settings.


### Government Digital Service - Senior Technology Advisor: Oct 2015 - Oct 2019 {#government-digital-service-senior-technology-advisor-oct-2015-oct-2019}

Focused on digital, data and technology transformation across
Government. Advisor on effective digital transformation strategy and
implementation. Leading on innovation strategy for GDS, developed
transformation support team and government transformation
strategy. Have assisted Treasury in Spending Review 2015 and Cabinet
Office in reviewing DWP’s technology, digital and data plans
incorporating effective transformation detail into their Single
Department Plan. Coached and supported upskilling of staff in agile
project delivery.


### BBC - Senior Technical Architect: June 2008 - Sept 2015 {#bbc-senior-technical-architect-june-2008-sept-2015}

Owner of the technical designs and roadmaps of multiple BBC Online
products including the BBC Homepage, Search, Navigation, Travel News
and location-based services. Combined, these provide key content
discovery mechanisms across all BBC products including News, Sport and
iPlayer on desktop, mobile and TV devices. I introduced personalised
audience discovery of content by targeting users with recommendations
and marketing campaigns via bespoke tools for Editorial and Marketing
teams.  Prior to this I was responsible for the Technology across the
BBC Knowledge and Learning portfolio including BBC Food, Nature,
Science and History, Bitesize, Languages and Class Clips.​ I managed
multidisciplinary teams in UX and development and rolled out agile
delivery practices across BBC vision teams, coaching technical and
non-technical people in Scrum.


### Previous positions (1999 - 2008) {#previous-positions--1999-2008}

-   Design Tab (various websites) - Director: 2007/08
-   Popart UK (e-commerce system) - Lead Engineer: 20
-   ZZY Media (mobile WAP and SMS) - Technical Lead: 2004/06
-   Common Sense Solutions (EPOS) - Development Engineer: 2003/04
-   Amersham Medical Systems (Radiology software) - Support Engineer: 2001/03
-   Microsoft (MSN chat, messenger and hotmail) - Internship: 1999/2000


## <span class="org-todo done DONE">DONE</span> Qualifications {#qualifications}

-   Level 3 Counselling Skills (ongoing)
-   Level 2 Counselling Skills (2019/20)
-   BBC Leadership Training (2011/12)
-   HND Computing Mathematics (1999)
